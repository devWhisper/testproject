﻿namespace HuaweiAPI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ctrlLastUpdate = new DevExpress.XtraEditors.TextEdit();
            this.ctrlMainTab = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageStations = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.ctrlTotalPower = new DevExpress.XtraEditors.TextEdit();
            this.ctrlTotalMonthPower = new DevExpress.XtraEditors.TextEdit();
            this.ctrlTotalCapacity = new DevExpress.XtraEditors.TextEdit();
            this.ctrlTotalDailyPower = new DevExpress.XtraEditors.TextEdit();
            this.grcStations = new DevExpress.XtraGrid.GridControl();
            this.bsStations = new System.Windows.Forms.BindingSource(this.components);
            this.grvStations = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repStation = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colprojectName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstationLinkman = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collinkmanPho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcapacity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbuildState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colaidType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDevices = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repDev = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.loTotalDailyPower = new DevExpress.XtraLayout.LayoutControlItem();
            this.loTotalCapacity = new DevExpress.XtraLayout.LayoutControlItem();
            this.M = new DevExpress.XtraLayout.LayoutControlItem();
            this.loTotalPower = new DevExpress.XtraLayout.LayoutControlItem();
            this.ctrlUpdateBtn = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.loUpdateBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.loMainTab = new DevExpress.XtraLayout.LayoutControlItem();
            this.loLastUpdate = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlLastUpdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMainTab)).BeginInit();
            this.ctrlMainTab.SuspendLayout();
            this.tabPageStations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalMonthPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalDailyPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalDailyPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loUpdateBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMainTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loLastUpdate)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ctrlLastUpdate);
            this.layoutControl1.Controls.Add(this.ctrlMainTab);
            this.layoutControl1.Controls.Add(this.ctrlUpdateBtn);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(2844, 1002);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ctrlLastUpdate
            // 
            this.ctrlLastUpdate.Location = new System.Drawing.Point(420, 12);
            this.ctrlLastUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlLastUpdate.Name = "ctrlLastUpdate";
            this.ctrlLastUpdate.Properties.ReadOnly = true;
            this.ctrlLastUpdate.Size = new System.Drawing.Size(211, 26);
            this.ctrlLastUpdate.StyleController = this.layoutControl1;
            this.ctrlLastUpdate.TabIndex = 6;
            // 
            // ctrlMainTab
            // 
            this.ctrlMainTab.Location = new System.Drawing.Point(12, 48);
            this.ctrlMainTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlMainTab.Name = "ctrlMainTab";
            this.ctrlMainTab.SelectedTabPage = this.tabPageStations;
            this.ctrlMainTab.Size = new System.Drawing.Size(2820, 942);
            this.ctrlMainTab.TabIndex = 5;
            this.ctrlMainTab.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageStations});
            // 
            // tabPageStations
            // 
            this.tabPageStations.Controls.Add(this.layoutControl2);
            this.tabPageStations.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageStations.Name = "tabPageStations";
            this.tabPageStations.Size = new System.Drawing.Size(2818, 900);
            this.tabPageStations.Text = "ΦΒ Πάρκα";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.ctrlTotalPower);
            this.layoutControl2.Controls.Add(this.ctrlTotalMonthPower);
            this.layoutControl2.Controls.Add(this.ctrlTotalCapacity);
            this.layoutControl2.Controls.Add(this.ctrlTotalDailyPower);
            this.layoutControl2.Controls.Add(this.grcStations);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(2818, 900);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // ctrlTotalPower
            // 
            this.ctrlTotalPower.Location = new System.Drawing.Point(1568, 42);
            this.ctrlTotalPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTotalPower.Name = "ctrlTotalPower";
            this.ctrlTotalPower.Size = new System.Drawing.Size(1238, 26);
            this.ctrlTotalPower.StyleController = this.layoutControl2;
            this.ctrlTotalPower.TabIndex = 8;
            // 
            // ctrlTotalMonthPower
            // 
            this.ctrlTotalMonthPower.Location = new System.Drawing.Point(170, 42);
            this.ctrlTotalMonthPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTotalMonthPower.Name = "ctrlTotalMonthPower";
            this.ctrlTotalMonthPower.Size = new System.Drawing.Size(1236, 26);
            this.ctrlTotalMonthPower.StyleController = this.layoutControl2;
            this.ctrlTotalMonthPower.TabIndex = 7;
            // 
            // ctrlTotalCapacity
            // 
            this.ctrlTotalCapacity.Location = new System.Drawing.Point(170, 12);
            this.ctrlTotalCapacity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTotalCapacity.Name = "ctrlTotalCapacity";
            this.ctrlTotalCapacity.Properties.ReadOnly = true;
            this.ctrlTotalCapacity.Size = new System.Drawing.Size(1236, 26);
            this.ctrlTotalCapacity.StyleController = this.layoutControl2;
            this.ctrlTotalCapacity.TabIndex = 6;
            // 
            // ctrlTotalDailyPower
            // 
            this.ctrlTotalDailyPower.Location = new System.Drawing.Point(1568, 12);
            this.ctrlTotalDailyPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTotalDailyPower.Name = "ctrlTotalDailyPower";
            this.ctrlTotalDailyPower.Properties.ReadOnly = true;
            this.ctrlTotalDailyPower.Size = new System.Drawing.Size(1238, 26);
            this.ctrlTotalDailyPower.StyleController = this.layoutControl2;
            this.ctrlTotalDailyPower.TabIndex = 5;
            // 
            // grcStations
            // 
            this.grcStations.DataSource = this.bsStations;
            this.grcStations.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcStations.Location = new System.Drawing.Point(12, 72);
            this.grcStations.MainView = this.grvStations;
            this.grcStations.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcStations.Name = "grcStations";
            this.grcStations.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repStation,
            this.repDev});
            this.grcStations.Size = new System.Drawing.Size(2794, 816);
            this.grcStations.TabIndex = 4;
            this.grcStations.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvStations});
            // 
            // bsStations
            // 
            this.bsStations.DataSource = typeof(HuaweiAPI.Utils.station);
            // 
            // grvStations
            // 
            this.grvStations.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIndex,
            this.colstationName,
            this.colprojectName,
            this.colstationLinkman,
            this.collinkmanPho,
            this.colcapacity,
            this.colbuildState,
            this.colaidType,
            this.colDevices});
            this.grvStations.DetailHeight = 538;
            this.grvStations.FixedLineWidth = 3;
            this.grvStations.GridControl = this.grcStations;
            this.grvStations.Name = "grvStations";
            this.grvStations.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.grvStations.OptionsView.EnableAppearanceOddRow = true;
            this.grvStations.OptionsView.ShowDetailButtons = false;
            this.grvStations.OptionsView.ShowFooter = true;
            this.grvStations.OptionsView.ShowGroupPanel = false;
            this.grvStations.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvStations_CustomDrawCell);
            // 
            // colIndex
            // 
            this.colIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.Caption = "AA";
            this.colIndex.MinWidth = 30;
            this.colIndex.Name = "colIndex";
            this.colIndex.Visible = true;
            this.colIndex.VisibleIndex = 0;
            this.colIndex.Width = 120;
            // 
            // colstationName
            // 
            this.colstationName.AppearanceHeader.Options.UseTextOptions = true;
            this.colstationName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colstationName.Caption = "Κωδικός";
            this.colstationName.ColumnEdit = this.repStation;
            this.colstationName.FieldName = "stationName";
            this.colstationName.MinWidth = 30;
            this.colstationName.Name = "colstationName";
            this.colstationName.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colstationName.Visible = true;
            this.colstationName.VisibleIndex = 1;
            this.colstationName.Width = 258;
            // 
            // repStation
            // 
            this.repStation.AutoHeight = false;
            this.repStation.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight)});
            this.repStation.Name = "repStation";
            this.repStation.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repStation_ButtonClick);
            // 
            // colprojectName
            // 
            this.colprojectName.AppearanceHeader.Options.UseTextOptions = true;
            this.colprojectName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colprojectName.Caption = "Περιγραφή";
            this.colprojectName.FieldName = "projectName";
            this.colprojectName.MinWidth = 30;
            this.colprojectName.Name = "colprojectName";
            this.colprojectName.Visible = true;
            this.colprojectName.VisibleIndex = 2;
            this.colprojectName.Width = 963;
            // 
            // colstationLinkman
            // 
            this.colstationLinkman.AppearanceHeader.Options.UseTextOptions = true;
            this.colstationLinkman.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colstationLinkman.Caption = "Υπεύθυνος Φ/Β";
            this.colstationLinkman.FieldName = "stationLinkman";
            this.colstationLinkman.MinWidth = 30;
            this.colstationLinkman.Name = "colstationLinkman";
            this.colstationLinkman.Visible = true;
            this.colstationLinkman.VisibleIndex = 3;
            this.colstationLinkman.Width = 274;
            // 
            // collinkmanPho
            // 
            this.collinkmanPho.AppearanceHeader.Options.UseTextOptions = true;
            this.collinkmanPho.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.collinkmanPho.Caption = "Επαφή Φ/Β";
            this.collinkmanPho.FieldName = "linkmanPho";
            this.collinkmanPho.MinWidth = 30;
            this.collinkmanPho.Name = "collinkmanPho";
            this.collinkmanPho.Visible = true;
            this.collinkmanPho.VisibleIndex = 4;
            this.collinkmanPho.Width = 274;
            // 
            // colcapacity
            // 
            this.colcapacity.AppearanceHeader.Options.UseTextOptions = true;
            this.colcapacity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colcapacity.Caption = "Ισχύς (KW)";
            this.colcapacity.FieldName = "capacity";
            this.colcapacity.MinWidth = 30;
            this.colcapacity.Name = "colcapacity";
            this.colcapacity.Visible = true;
            this.colcapacity.VisibleIndex = 5;
            this.colcapacity.Width = 274;
            // 
            // colbuildState
            // 
            this.colbuildState.AppearanceHeader.Options.UseTextOptions = true;
            this.colbuildState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colbuildState.Caption = "Κατάσταση";
            this.colbuildState.FieldName = "sbuildState";
            this.colbuildState.MinWidth = 30;
            this.colbuildState.Name = "colbuildState";
            this.colbuildState.Visible = true;
            this.colbuildState.VisibleIndex = 6;
            this.colbuildState.Width = 370;
            // 
            // colaidType
            // 
            this.colaidType.AppearanceHeader.Options.UseTextOptions = true;
            this.colaidType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colaidType.Caption = "Τύπος Φ/Β";
            this.colaidType.FieldName = "aidType";
            this.colaidType.MinWidth = 30;
            this.colaidType.Name = "colaidType";
            this.colaidType.Visible = true;
            this.colaidType.VisibleIndex = 7;
            this.colaidType.Width = 193;
            // 
            // colDevices
            // 
            this.colDevices.AppearanceHeader.Options.UseTextOptions = true;
            this.colDevices.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDevices.Caption = "Εξοπλισμός";
            this.colDevices.ColumnEdit = this.repDev;
            this.colDevices.MinWidth = 30;
            this.colDevices.Name = "colDevices";
            this.colDevices.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDevices.Visible = true;
            this.colDevices.VisibleIndex = 8;
            this.colDevices.Width = 112;
            // 
            // repDev
            // 
            this.repDev.AutoHeight = false;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repDev.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repDev.Name = "repDev";
            this.repDev.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repDev_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.loTotalDailyPower,
            this.loTotalCapacity,
            this.M,
            this.loTotalPower});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(2818, 900);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.grcStations;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(2798, 820);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // loTotalDailyPower
            // 
            this.loTotalDailyPower.Control = this.ctrlTotalDailyPower;
            this.loTotalDailyPower.Location = new System.Drawing.Point(1398, 0);
            this.loTotalDailyPower.Name = "loTotalDailyPower";
            this.loTotalDailyPower.Size = new System.Drawing.Size(1400, 30);
            this.loTotalDailyPower.Text = "Ημερήσια Παραγωγή";
            this.loTotalDailyPower.TextSize = new System.Drawing.Size(155, 20);
            // 
            // loTotalCapacity
            // 
            this.loTotalCapacity.Control = this.ctrlTotalCapacity;
            this.loTotalCapacity.Location = new System.Drawing.Point(0, 0);
            this.loTotalCapacity.Name = "loTotalCapacity";
            this.loTotalCapacity.Size = new System.Drawing.Size(1398, 30);
            this.loTotalCapacity.Text = "Εγκατεστημένη Ισχύς";
            this.loTotalCapacity.TextSize = new System.Drawing.Size(155, 20);
            // 
            // M
            // 
            this.M.Control = this.ctrlTotalMonthPower;
            this.M.Location = new System.Drawing.Point(0, 30);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(1398, 30);
            this.M.Text = "Μηνιαία Παραγωγή";
            this.M.TextSize = new System.Drawing.Size(155, 20);
            // 
            // loTotalPower
            // 
            this.loTotalPower.Control = this.ctrlTotalPower;
            this.loTotalPower.Location = new System.Drawing.Point(1398, 30);
            this.loTotalPower.Name = "loTotalPower";
            this.loTotalPower.Size = new System.Drawing.Size(1400, 30);
            this.loTotalPower.Text = "Συνολική Παραγωγή";
            this.loTotalPower.TextSize = new System.Drawing.Size(155, 20);
            // 
            // ctrlUpdateBtn
            // 
            this.ctrlUpdateBtn.Location = new System.Drawing.Point(12, 12);
            this.ctrlUpdateBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlUpdateBtn.Name = "ctrlUpdateBtn";
            this.ctrlUpdateBtn.Size = new System.Drawing.Size(246, 32);
            this.ctrlUpdateBtn.StyleController = this.layoutControl1;
            this.ctrlUpdateBtn.TabIndex = 4;
            this.ctrlUpdateBtn.Text = "Update Data";
            this.ctrlUpdateBtn.Click += new System.EventHandler(this.ctrlUpdateBtn_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.loUpdateBtn,
            this.emptySpaceItem2,
            this.loMainTab,
            this.loLastUpdate});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(2844, 1002);
            this.Root.TextVisible = false;
            // 
            // loUpdateBtn
            // 
            this.loUpdateBtn.Control = this.ctrlUpdateBtn;
            this.loUpdateBtn.Location = new System.Drawing.Point(0, 0);
            this.loUpdateBtn.Name = "loUpdateBtn";
            this.loUpdateBtn.Size = new System.Drawing.Size(250, 36);
            this.loUpdateBtn.TextSize = new System.Drawing.Size(0, 0);
            this.loUpdateBtn.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(623, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(2201, 36);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // loMainTab
            // 
            this.loMainTab.Control = this.ctrlMainTab;
            this.loMainTab.Location = new System.Drawing.Point(0, 36);
            this.loMainTab.Name = "loMainTab";
            this.loMainTab.Size = new System.Drawing.Size(2824, 946);
            this.loMainTab.TextSize = new System.Drawing.Size(0, 0);
            this.loMainTab.TextVisible = false;
            // 
            // loLastUpdate
            // 
            this.loLastUpdate.Control = this.ctrlLastUpdate;
            this.loLastUpdate.Location = new System.Drawing.Point(250, 0);
            this.loLastUpdate.Name = "loLastUpdate";
            this.loLastUpdate.Size = new System.Drawing.Size(373, 36);
            this.loLastUpdate.Text = "Τελευταία ενημέρωση";
            this.loLastUpdate.TextSize = new System.Drawing.Size(155, 20);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2844, 1002);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctrlLastUpdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMainTab)).EndInit();
            this.ctrlMainTab.ResumeLayout(false);
            this.tabPageStations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalMonthPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalDailyPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalDailyPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loUpdateBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMainTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loLastUpdate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraTab.XtraTabControl ctrlMainTab;
        private DevExpress.XtraTab.XtraTabPage tabPageStations;
        private DevExpress.XtraEditors.SimpleButton ctrlUpdateBtn;
        private DevExpress.XtraLayout.LayoutControlItem loUpdateBtn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem loMainTab;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl grcStations;
        private DevExpress.XtraGrid.Views.Grid.GridView grvStations;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource bsStations;
        private DevExpress.XtraGrid.Columns.GridColumn colIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colstationName;
        private DevExpress.XtraGrid.Columns.GridColumn colstationLinkman;
        private DevExpress.XtraGrid.Columns.GridColumn collinkmanPho;
        private DevExpress.XtraGrid.Columns.GridColumn colcapacity;
        private DevExpress.XtraGrid.Columns.GridColumn colbuildState;
        private DevExpress.XtraGrid.Columns.GridColumn colaidType;
        private DevExpress.XtraEditors.TextEdit ctrlLastUpdate;
        private DevExpress.XtraLayout.LayoutControlItem loLastUpdate;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repStation;
        private DevExpress.XtraEditors.TextEdit ctrlTotalDailyPower;
        private DevExpress.XtraLayout.LayoutControlItem loTotalDailyPower;
        private DevExpress.XtraEditors.TextEdit ctrlTotalCapacity;
        private DevExpress.XtraLayout.LayoutControlItem loTotalCapacity;
        private DevExpress.XtraEditors.TextEdit ctrlTotalPower;
        private DevExpress.XtraEditors.TextEdit ctrlTotalMonthPower;
        private DevExpress.XtraLayout.LayoutControlItem M;
        private DevExpress.XtraLayout.LayoutControlItem loTotalPower;
        private DevExpress.XtraGrid.Columns.GridColumn colprojectName;
        private DevExpress.XtraGrid.Columns.GridColumn colDevices;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repDev;
    }
}

