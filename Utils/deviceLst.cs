﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;
using SLnet.Base.DbUtils;

namespace HuaweiAPI.Utils
{
    public class deviceLst
    {
        public List<station> stations { get; set; }
        public Connection connection { get; set; }

        public deviceLst(auth authenticator, Connection connection)
        {
            stationLst sList = new stationLst(authenticator, connection);
            List<station> stations = sList.stationFactory();
            this.stations = sList.updateStations(stations);
            this.connection = connection;
        }//deviceLst

        public deviceLst(List<station> stations)
        {
            this.stations = stations;
        }//deviceLst

        public void postDevicesAlarms()
        {
            List<deviceAlarm> alarms = getAlarmList();
            foreach(deviceAlarm a in alarms)
            {
                slQueryParameters prms = new slQueryParameters();
                prms.Add("@alarmID", a.alarmId, DbType.String);
                prms.Add("@stationCode", a.stationCode, DbType.String);
                prms.Add("@esnCode", a.esnCode, DbType.String);
                DataTable dt = connection.ExecuteResultSet("Select * From ETA05PVMONITORDEVICEALARM where eta05alarmID = @alarmID and ETA05STATIONCODE = @stationCode and eta05esncode = @esnCode", prms); 
                if (dt.Rows.Count <= 0)
                {
                    Guid stationID = Guid.Empty;
                    prms = new slQueryParameters("@stationCode", a.stationCode);
                    DataTable ddt = connection.ExecuteResultSet("Select * From etA05PvMonitorStations where etA05StationCode = @stationCode", prms);
                    if (ddt.Rows.Count > 0)
                        stationID = ddt.Rows[0]["etA05ID"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["etA05ID"].ToString()) ? Guid.Parse(ddt.Rows[0]["etA05ID"].ToString()) : Guid.Empty;
                    prms = new slQueryParameters();
                    prms.Add("@etA05stationCode", a.stationCode, DbType.String);
                    prms.Add("@etA05alarmName", a.alarmName, DbType.String);
                    prms.Add("@etA05devName", a.devName, DbType.String);
                    prms.Add("@etA05repairSuggestion", a.repairSuggestion, DbType.String);
                    prms.Add("@etA05esnCode", a.esnCode, DbType.String);
                    prms.Add("@etA05devTypeId", a.devTypeId, DbType.Int64);
                    prms.Add("@etA05causeId", a.causeId, DbType.Int64);
                    prms.Add("@etA05alarmCause", a.alarmCause, DbType.String);
                    prms.Add("@etA05alarmType", a.alarmType, DbType.Int64);
                    prms.Add("@etA05raiseDateTime", a.raiseDateTime, DbType.DateTime);
                    prms.Add("@etA05alarmId", a.alarmId, DbType.Int64);
                    prms.Add("@etA05stationName", a.stationName, DbType.String);
                    prms.Add("@etA05lev", a.lev, DbType.Int64);
                    prms.Add("@etA05status", a.status, DbType.Int64);
                    prms.Add("@etA05stationID", stationID, DbType.Guid);
                    dt = connection.ExecuteResultSet(@" declare  @ID uniqueidentifier  = NEWID(); 
                                                            Insert  into ETA05PVMONITORDEVICEALARM (etA05ID, etA05stationCode, etA05alarmName, etA05devName, etA05repairSuggestion, etA05esnCode, etA05devTypeId, etA05causeId, etA05alarmCause, etA05alarmType, etA05raiseDateTime, etA05alarmId, etA05stationName, etA05lev, etA05status, etA05stationID) OUTPUT @ID  
                                                            values(@ID, @etA05stationCode, @etA05alarmName, @etA05devName, @etA05repairSuggestion, @etA05esnCode, @etA05devTypeId, @etA05causeId, @etA05alarmCause, @etA05alarmType, @etA05raiseDateTime, @etA05alarmId, @etA05stationName, @etA05lev, @etA05status, @etA05stationID)", prms);
                    if (dt.Rows.Count > 0)
                        a.dbid = dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) ? Guid.Parse(dt.Rows[0][0].ToString()) : Guid.Empty;
                }
                else if (dt.Rows.Count > 0)
                {
                    a.dbid = dt.Rows[0]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[0]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[0]["etA05ID"].ToString()) : Guid.Empty;
                }
            }
        }//postDevicesAlarms

        public void updateDevices()
        {
            List<device> devs = deviceFactory();
            foreach(device d in devs)
            {
                slQueryParameters prms = new slQueryParameters();
                prms.Add("@apidevid", d.id, DbType.String);
                DataTable dt = connection.ExecuteResultSet("Select * From eta05pvmonitordevices where eta05apidevid = @apidevid", prms);
                if (dt.Rows.Count <= 0)
                {
                    Guid stationID = Guid.Empty;
                    prms = new slQueryParameters("@stationCode", d.stationCode);
                    DataTable ddt = connection.ExecuteResultSet("Select * From etA05PvMonitorStations where etA05StationCode = @stationCode", prms);
                    if (ddt.Rows.Count > 0)
                        stationID = ddt.Rows[0]["etA05ID"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["etA05ID"].ToString()) ? Guid.Parse(ddt.Rows[0]["etA05ID"].ToString()) : Guid.Empty;
                    prms = new slQueryParameters();
                    prms.Add("@eta05APIDevId", d.id, DbType.String);
                    prms.Add("@eta05devName", d.devName, DbType.String);
                    prms.Add("@eta05stationCode", d.stationCode, DbType.String);
                    prms.Add("@eta05stationID", stationID, DbType.Guid);
                    prms.Add("@eta05esnCode", d.esnCode, DbType.String);
                    prms.Add("@eta05devTypeId", d.devTypeId, DbType.Int64);
                    prms.Add("@eta05softwareVersion", d.softwareVersion, DbType.String);
                    prms.Add("@eta05invType", d.invType, DbType.String);
                    prms.Add("@eta05longitude", d.longitude, DbType.String);
                    prms.Add("@eta05latitude", d.latitude, DbType.String);
                    dt = connection.ExecuteResultSet(@" declare  @ID uniqueidentifier  = NEWID(); 
                                                            Insert  into eta05pvmonitordevices (etA05ID, eta05APIDevId, eta05devName, eta05stationCode, eta05esnCode, eta05devTypeId, eta05softwareVersion, eta05invType, eta05longitude, eta05latitude, eta05StationID) OUTPUT @ID  
                                                            values(@ID, @eta05APIDevId, @eta05devName, @eta05stationCode, @eta05esnCode, @eta05devTypeId, @eta05softwareVersion, @eta05invType, @eta05longitude, @eta05latitude, @eta05stationID)", prms);
                    if (dt.Rows.Count > 0)
                        d.dbid = dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) ? Guid.Parse(dt.Rows[0][0].ToString()) : Guid.Empty;
                }
                else if (dt.Rows.Count > 0)
                {
                    d.dbid = dt.Rows[0]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[0]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[0]["etA05ID"].ToString()) : Guid.Empty;
                }

            }
        }//updateDevices

        public List<device> deviceFactory()
        {
            ///<summary> ---- Device List Interface ---- </summary>
            /// 
            /// This interface is used to obtain basic device information. Before opening the device data
            /// interfaces, you must configure this interface. You can query device information by plant ID.A
            /// maximum of 100 plants can be queried at a time.
            ///
            List<device> resp = new List<device>();
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(deviceFactory100(stations.GetRange(lowerLimit, upperLimit)));
            }
            return resp;
        }//deviceFactory

        public List<device> deviceFactory100(List<station> sstations)
        {
            string body = string.Empty;
            foreach (station s in sstations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            body = "{" + body + "}";
            List<device> collection = new List<device>();
            request r = new request(body, "getDevList", sstations.First().authenticator);
            if (!r.reqResponse.success || r.reqResponse.failCode == 20008)
                return collection;
            JArray rdt = r.reqResponse.data as JArray;
            foreach (JObject obj in rdt)
            {
                device d = JsonConvert.DeserializeObject<device>(obj.ToString());
                collection.Add(d);
            }
            return collection;
        }//deviceFactory100

        public List<deviceAlarm> getAlarmList()
        {
            List<deviceAlarm> resp = new List<deviceAlarm>();
            DateTime from = DateTime.Now.AddDays(-7);
            DateTime end = DateTime.Now;
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(getAlarmList100(stations.GetRange(lowerLimit, upperLimit), from, end));
            }
            return resp;
        }

        public List<deviceAlarm> getAlarmList100(List<station> sstations, DateTime from, DateTime end)
        {
            string body = string.Empty;
            foreach (station s in sstations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            body += ",\n\"beginTime\":\"" + Convert.ToInt64((from - DateTime.Parse("1/1/1970")).TotalMilliseconds).ToString() + "\"";
            body += ",\n\"endTime\":\"" + Convert.ToInt64((end - DateTime.Parse("1/1/1970")).TotalMilliseconds).ToString() + "\"";
            body += ",\n\"language\":\"en_UK\"";
            body = "{" + body + "}";
            List<deviceAlarm> collection = new List<deviceAlarm>();
            request r = new request(body, "getAlarmList", sstations.First().authenticator);
            if (!r.reqResponse.success || r.reqResponse.failCode == 20008)
                return collection;
            JArray rdt = r.reqResponse.data as JArray;
            foreach (JObject obj in rdt)
            {
                deviceAlarm d = JsonConvert.DeserializeObject<deviceAlarm>(obj.ToString());
                collection.Add(d);
            }
            return collection;
        }
    }
}
