﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using SLnet.Base.DbUtils;

namespace HuaweiAPI.Utils
{
    public class Connection
    {
        string userName { get; set; }
        string passWord { get; set; }
        string database { get; set; }
        string server { get; set; }
        public List<messageRow> messageHistory { get; set; }
        internal string connetionString { get; set; }
        public SqlConnection sqlObj { get; internal set; }
        public Connection(string server, string database, string userName, string passWord)
        {
            #region init 
            this.server = server;
            //Guid key = getKey();
            this.database = database;
            this.userName = userName;
            this.passWord = passWord;
            //string decryptedUser = StringCipher.Decrypt(userName, key.ToString());
            //string decryptedPass = StringCipher.Decrypt(passWord, key.ToString());
            this.messageHistory = new List<messageRow>();
            #endregion

            this.connetionString = "Data Source='" + this.server + "';Initial Catalog='" + this.database + "';User ID=" + this.userName + ";Password=" + this.passWord + ";MultipleActiveResultSets=true;";
        }

        public DataTable ExecuteResultSet(string command, slQueryParameters prms)
        {
            DataTable results = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = command;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlObj;
            TypeConvertor conv = new TypeConvertor();
            for (int i = 0; i < prms.Items.Count; i++)
            {
                cmd.Parameters.Add(prms.Items[i].Name.ToString(), conv.ToSqlDbType(prms.Items[i].DBType)).Value = prms.Items[i].Value;
            }

            try
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    results.Load(reader);
                }
            }
            catch (Exception ex)
            {
                writeEvent(ex.Message.Replace('\'', '"'));
            }
            return results;
        }//ExecuteSql

        public void writeEvent(string sevent)
        {
            slQueryParameters prms = new slQueryParameters();
            DataTable dt = ExecuteResultSet("Insert into ETA05PVMONITORLOG values(NEWID(), GETDATE(), '" + sevent + "')", prms);
        }//writeInterval


        public Connection(string connetionString)
        {
            this.messageHistory = new List<messageRow>();
            this.connetionString = connetionString;
        }

        public void connect()
        {
            sqlObj = new SqlConnection(connetionString);
            try
            {
                sqlObj.Open();
                string message = " The connection was successfully established on " + DateTime.Now.ToString() + ".";
                messageHistory.Add(new messageRow(message));
            }
            catch (Exception ex)
            {
                string message = "The connection could not be established on " + DateTime.Now.ToString() + ".\n";
                message += "The erros messages is:\n" + ex.ToString();
                messageHistory.Add(new messageRow(message));
            }
        }//connect

        public void disconnect()
        {
            if (sqlObj != null && sqlObj.State == ConnectionState.Open)
            {
                try
                {
                    sqlObj.Close();
                    messageHistory.Add(new messageRow("The connection closed successfully on " + DateTime.Now.ToString() + "."));
                }
                catch (Exception ex)
                {
                    string message = "The connection could not be closed successfully on " + DateTime.Now.ToString() + ".\n";
                    message += "The erros messages is:\n" + ex.ToString();
                    messageHistory.Add(new messageRow(message));
                }
            }
        }

        public class messageRow
        {
            public string message { get; set; }
            public DateTime time { get; set; }
            public messageRow(string message, DateTime time)
            {
                this.message = message;
                this.time = time;
            }
            public messageRow()
            {
                this.message = string.Empty;
                this.time = DateTime.Now;
            }
            public messageRow(string message)
            {
                this.message = message;
                this.time = DateTime.Now;
            }
        }
    }
}
