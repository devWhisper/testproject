﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SLnet.Base.DbUtils;
using System.Data.SqlClient;
using System.Data;

namespace HuaweiAPI.Utils
{
    public class station
    {
        public Guid stationID { get; set; }
        public string stationCode { get; set; }//Plant ID, which uniquely identifies a plant.
        public string stationName { get; set; }//Plant name
        public string stationAddr { get; set; }//Detailed address of the plant
        public double capacity { get; set; } //Installed capacity (unit: MW)
        public int _buildState { get; set; }
        public int buildState//Plant status => 0: Not built, 1: Under construction, 2: Grid-connected 
        {
            get
            {
                return _buildState;
            }
            set
            {
                _buildState = value;
                switch (value)
                {
                    case 1:
                        sbuildState = "Δεν έχει ξεκινήσει";
                        break;
                    case 2:
                        sbuildState = "Υπο κατασκευή";
                        break;
                    case 3:
                        sbuildState = "Ολοκληρωμένη κατασκευή - Συνδεδεμένο στο δίκτυο";
                        break;
                    default:
                        sbuildState = string.Empty;
                        break;
                }
            }
        }
        public string sbuildState { get; set; }
        public int combineType { get; set; }//Grid connection type 1: Utility, 2: C&I plant, 3: Residential plant
        public int aidType { get; set; }//Poverty alleviation plant flag 0: Poverty alleviation plant, 1: Not poverty alleviation plant
        public string stationLinkman { get; set; }//Plant contact
        public string linkmanPho { get; set; }//Contact phone number
        public auth authenticator { get; set; }
        public kpi realkpi { get; set; }
        public List<hkpi> hkpi { get; set; }
        public List<dkpi> dkpi { get; set; }
        public List<dkpi> mkpi { get; set; }
        public List<dkpi> ykpi { get; set; }
        public string projectName { get; set; }
        public station()
        {

        }//station

        public List<device> devFactoryLst(Connection connection)
        {
            List<device> dummyLst = new List<device>();
            string sql = @" select * from eta05pvmonitordevices
                            where 1=1
                            and ETA05STATIONID = @stationID";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@stationID", stationID, DbType.Guid);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    device d = new device();
                    d.dbid = dt.Rows[i]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[i]["etA05ID"].ToString()) : Guid.Empty;
                    d.devName = dt.Rows[i]["etA05devName"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05devName"].ToString()) ? dt.Rows[i]["etA05devName"].ToString() : string.Empty;
                    d.devTypeId = dt.Rows[i]["etA05devTypeId"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05devTypeId"].ToString()) ? int.Parse(dt.Rows[i]["etA05devTypeId"].ToString()) : -1;
                    d.esnCode = dt.Rows[i]["etA05esnCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05esnCode"].ToString()) ? dt.Rows[i]["etA05esnCode"].ToString() : string.Empty;
                    d.id = dt.Rows[i]["etA05apiDevid"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05apiDevid"].ToString()) ? dt.Rows[i]["etA05apiDevid"].ToString() : string.Empty;
                    d.invType = dt.Rows[i]["eta05invType"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05invType"].ToString()) ? dt.Rows[i]["eta05invType"].ToString() : string.Empty;
                    d.latitude = dt.Rows[i]["eta05latitude"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05latitude"].ToString()) ? dt.Rows[i]["eta05latitude"].ToString() : string.Empty;
                    d.longitude = dt.Rows[i]["eta05longitude"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05longitude"].ToString()) ? dt.Rows[i]["eta05longitude"].ToString() : string.Empty;
                    d.softwareVersion = dt.Rows[i]["etA05softwareVersion"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05softwareVersion"].ToString()) ? dt.Rows[i]["etA05softwareVersion"].ToString() : string.Empty;
                    d.stationCode = dt.Rows[i]["etA05stationCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05stationCode"].ToString()) ? dt.Rows[i]["etA05stationCode"].ToString() : string.Empty;
                    dummyLst.Add(d);
                }
            }
            return dummyLst;
        }//devFactoryLst
    }//station

    public class dKpiDataItem
    {
        public object use_power { get; set; }// Power consumption (kWh)
        public object radiation_intensity { get; set; }// Total irradiation (kWh/m²)
        public object reduction_total_co2 { get; set; }// CO2 reduction (Ton)
        public object reduction_total_coal { get; set; }// Standard coal savings (Ton)
        public object theory_power { get; set; }// Theoretical energy (kWh)
        public object ongrid_power { get; set; }// Feed-in energy (kWh)
        public object power_profit { get; set; }// Energy revenue (¥ (Currency conversion is not performed.))
        public object installed_capacity { get; set; }// Installed capacity (kW)
        public object perpower_ratio { get; set; }// Equivalent utilization hours (h)
        public object inverter_power { get; set; }// Inverter energy (kWh)
        public object reduction_total_tree { get; set; }// Equivalent tree planting (Tree)
        public object performance_ratio { get; set; }// Electricity generation efficiency (kWh)
        public DateTime collectTime { get; set; }
        public double _use_power
        {
            get { return use_power != null ? double.Parse(use_power.ToString()) : 0; }
            set { use_power = value; }
        }
        public double _radiation_intensity
        {
            get { return radiation_intensity != null ? double.Parse(radiation_intensity.ToString()) : 0; }
            set { radiation_intensity = value; }
        }
        public double _reduction_total_co2
        {
            get { return reduction_total_co2 != null ? double.Parse(reduction_total_co2.ToString()) : 0; }
            set { reduction_total_co2 = value; }
        }
        public double _reduction_total_coal
        {
            get { return reduction_total_coal != null ? double.Parse(reduction_total_coal.ToString()) : 0; }
            set { reduction_total_coal = value; }
        }
        public double _theory_power
        {
            get { return theory_power != null ? double.Parse(theory_power.ToString()) : 0; }
            set { theory_power = value; }
        }
        public double _ongrid_power
        {
            get { return ongrid_power != null ? double.Parse(ongrid_power.ToString()) : 0; }
            set { ongrid_power = value; }
        }
        public double _power_profit
        {
            get { return power_profit != null ? double.Parse(power_profit.ToString()) : 0; }
            set { power_profit = value; }
        }
        public double _installed_capacity
        {
            get { return installed_capacity != null ? double.Parse(installed_capacity.ToString()) : 0; }
            set { installed_capacity = value; }
        }
        public double _perpower_ratio
        {
            get { return perpower_ratio != null ? double.Parse(perpower_ratio.ToString()) : 0; }
            set { perpower_ratio = value; }
        }
        public double _inverter_power
        {
            get { return inverter_power != null ? double.Parse(inverter_power.ToString()) : 0; }
            set { inverter_power = value; }
        }
        public int _reduction_total_tree
        {
            get { return reduction_total_tree != null ? int.Parse(reduction_total_tree.ToString()) : 0; }
            set { reduction_total_tree = value; }
        }
        public double _performance_ratio
        {
            get { return performance_ratio != null ? double.Parse(performance_ratio.ToString()) : 0; }
            set { performance_ratio = value; }
        }
    }//dKpiDataItem

    public class hKpiDataItem
    {
        public object radiation_intensity { get; set; } //Total irradiation (kWh/m²)
        public object theory_power { get; set; } //Theoretical energy (kWh)
        public object inverter_power { get; set; } //Inverter energy (kWh)
        public object ongrid_power { get; set; } //Feed-in energy (kWh)
        public object power_profit { get; set; } //Energy revenue (¥ (Currency conversion is not performed.))
        public DateTime collectTime { get; set; }
        public double _radiation_intensity 
        { 
            get { return radiation_intensity != null ? double.Parse(radiation_intensity.ToString()) : 0; } 
            set { radiation_intensity = value; } 
        } 
        public double _theory_power
        {
            get { return theory_power != null ? double.Parse(theory_power.ToString()) : 0; }
            set { theory_power = value; }
        }
        public double _inverter_power
        {
            get { return inverter_power != null ? double.Parse(inverter_power.ToString()) : 0; }
            set { inverter_power = value; }
        }
        public double _ongrid_power
        {
            get { return ongrid_power != null ? double.Parse(ongrid_power.ToString()) : 0; }
            set { ongrid_power = value; }
        }
        public double _power_profit
        {
            get { return power_profit != null ? double.Parse(power_profit.ToString()) : 0; }
            set { power_profit = value; }
        }
    }//hKpiDataItem

    public class kpiDataItem
    {
        public double day_power { get; set; }//Daily energy (kWh)
        public double month_power { get; set; }//Monthly energy (kWh)
        public double total_power { get; set; }//Lifetime energy (kWh)
        public double day_income { get; set; }//Daily revenue --> ¥ (Currency conversion is not performed.)
        public double total_income { get; set; }//Total revenue --> ¥ (Currency conversion is not performed.)
        public int _real_health_state { get; set; }//Plant status - None (Plant status: 1: Disconnected 2: Faulty 3: Healthy)
        public int real_health_state
        {
            get { return _real_health_state; }
            set { 
                _real_health_state = value; 
                switch (value)
                {
                    case 1:
                        sreal_health_state = "Αποσυνδεμένο";
                        break;
                    case 2:
                        sreal_health_state = "Υπάρχει Βλάβη";
                        break;
                    case 3:
                        sreal_health_state = "Κανονική Λειτουργία";
                        break;
                    default:
                        sreal_health_state = string.Empty;
                        break;
                }
            }
        }
        public string sreal_health_state { get; set; }
    }//kpiDataItem

    public class kpi
    {
        public kpiDataItem dataItemMap { get; set; }
        public string stationCode { get; set; }
        public string errorMessage { get; set; }
        public kpi()
        {
            dataItemMap = new kpiDataItem();
        }
    }//statistics

    public class hkpi
    {
        public hKpiDataItem dataItemMap { get; set; }
        public string stationCode { get; set; }
        public DateTime collectDateTime { get; set; }
        public long _collectTime { get; set; }
        public long collectTime { 
            get { return _collectTime; } 
            set 
            { 
                _collectTime = value;
                collectDateTime = DateTime.Parse("1/1/1970").AddMilliseconds(value);
            } 
        }
        public string errorMessage { get; set; }
    }

    public class dkpi
    {
        public dKpiDataItem dataItemMap { get; set; }
        public string stationCode { get; set; }
        public DateTime collectDateTime { get; set; }
        public long _collectTime { get; set; }
        public long collectTime
        {
            get { return _collectTime; }
            set
            {
                _collectTime = value;
                collectDateTime = DateTime.Parse("1/1/1970").AddMilliseconds(value);
            }
        }
        public string errorMessage { get; set; }
    }

    public class mkpi
    {
        public dKpiDataItem dataItemMap { get; set; }
        public string stationCode { get; set; }
        public DateTime collectDateTime { get; set; }
        public long _collectTime { get; set; }
        public long collectTime
        {
            get { return _collectTime; }
            set
            {
                _collectTime = value;
                collectDateTime = DateTime.Parse("1/1/1970").AddMilliseconds(value);
            }
        }
        public string errorMessage { get; set; }
    }

    public class ykpi
    {
        public dKpiDataItem dataItemMap { get; set; }
        public string stationCode { get; set; }
        public DateTime collectDateTime { get; set; }
        public long _collectTime { get; set; }
        public long collectTime
        {
            get { return _collectTime; }
            set
            {
                _collectTime = value;
                collectDateTime = DateTime.Parse("1/1/1970").AddMilliseconds(value);
            }
        }
        public string errorMessage { get; set; }
    }
}
