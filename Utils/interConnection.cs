﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Security;
using org.apache.pdfbox.pdmodel;
using HuaweiAPI.Utils;
using SLnet.Base.DbUtils;
using HuaweiAPI.Forms;

namespace HuaweiAPI.Utils
{
    public class interConnection
    {
        public Connection connection { get; set; }
        public string connStr { get; set; }

        public interConnection(string connStr) 
        {
            this.connStr = connStr;
            connection = new Connection(connStr);
        }

        public bool checkInterval()
        {
            bool resp = false;
            string sevent = "New API connection";
            slQueryParameters prms = new slQueryParameters("@eventType", sevent);
            DataTable dt = connection.ExecuteResultSet("SELECT * FROM ETA05PVMONITORLOG where 1=1 and etA05EventType = @eventType order by etA05DatetimeEvent desc", prms);
            if (dt.Rows.Count == 0 || (dt.Rows.Count > 0 && (DateTime.Now - DateTime.Parse(dt.Rows[0]["etA05DatetimeEvent"].ToString())).TotalMinutes > 1))
                resp = true;
            return resp;
        }//checkInterval

    }
}
