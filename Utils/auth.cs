﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Security;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using spc = Microsoft.SharePoint.Client;
using System.IO;
using System.Net;
using HuaweiAPI.Utils;
using Newtonsoft.Json;
using System.Web;

namespace HuaweiAPI.Utils
{
    public class auth
    {
        public string url { get; set; }
        public string userName { get; set; }
        public string systemCode { get; set; }
        public string XSRFTOKEN { get; set; }
        public CookieContainer cookieContainer { get; set; }

        public auth()
        {
            url = string.Empty;
            userName = string.Empty;
            systemCode = string.Empty;
        }//auth

        public auth(string url, string userName, string systemCode)
        {
            this.url = url;
            this.userName = userName;
            this.systemCode = systemCode;
        }//auth

        public void pushLogin()
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url + "login");
            cookieContainer = new CookieContainer();
            httpWebRequest.CookieContainer = cookieContainer;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(this);
                streamWriter.Write(json);
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    response resp = new response();
                    resp.responseFactory(result);
                    if (resp.success)
                    {
                        XSRFTOKEN = httpResponse.Headers[HttpResponseHeader.SetCookie] != null && !string.IsNullOrEmpty(httpResponse.Headers[HttpResponseHeader.SetCookie]) ? httpResponse.Headers[HttpResponseHeader.SetCookie].Replace("HttpOnly,", "") : string.Empty;
                        XSRFTOKEN = !string.IsNullOrEmpty(XSRFTOKEN) ? (XSRFTOKEN.Split(';').Length > 0 && XSRFTOKEN.Split(';').ToList().Where(x => x.Contains("XSRF")).Any() ? XSRFTOKEN.Split(';').ToList().Where(x => x.Contains("XSRF")).First() : string.Empty) : string.Empty;
                        XSRFTOKEN = !string.IsNullOrEmpty(XSRFTOKEN) ? (XSRFTOKEN.Split('=').Length > 0 ? XSRFTOKEN.Split('=')[1] : string.Empty) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }//makeLogin

    }
}
