﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;
using SLnet.Base.DbUtils;

namespace HuaweiAPI.Utils
{
    public class stationLst
    {
        public auth authenticator { get; set; }
        public Connection connection { get; set; }

        public stationLst(auth authenticator, Connection connection)
        {
            this.authenticator = authenticator;
            this.connection = connection;
        }//listFactory

        public void postKPIs(List<station> stations)
        {

            #region Real KPIs
            getStationRealKpi(stations); 
            slQueryParameters prms;
            DateTime collectionDatetime = DateTime.Now;
            foreach (station s in stations)
            {
                prms = new slQueryParameters();
                prms.Add("@etA05Day_power", s.realkpi.dataItemMap.day_power, DbType.Double);
                prms.Add("@etA05Month_power", s.realkpi.dataItemMap.month_power, DbType.Double);
                prms.Add("@etA05Total_power", s.realkpi.dataItemMap.total_power, DbType.Double);
                prms.Add("@etA05Day_income", s.realkpi.dataItemMap.day_income, DbType.Double);
                prms.Add("@etA05Total_income", s.realkpi.dataItemMap.total_income, DbType.Double);
                prms.Add("@etA05real_health_state", s.realkpi.dataItemMap.real_health_state, DbType.Double);
                prms.Add("@etA05StationID", s.stationID, DbType.Guid);
                prms.Add("@etA05CollectDatetime", collectionDatetime, DbType.DateTime);
                connection.ExecuteResultSet(@"Insert into etA05PVMonitorRealKPI(etA05ID, etA05Day_power, etA05Month_power, etA05Total_power, etA05Day_income, etA05Total_income, etA05real_health_state, etA05StationID, etA05CollectDatetime) 
                                                  values(NEWID(), @etA05Day_power, @etA05Month_power, @etA05Total_power, @etA05Day_income, @etA05Total_income, @etA05real_health_state, @etA05StationID, @etA05CollectDatetime)", prms);
            }
            #endregion

            #region Hour KPIs
            List<hkpi> hkpis = getKpiStationHour(stations, collectionDatetime);
            foreach (hkpi k in hkpis)
            {
                station s = stations.Where(x => x.stationCode == k.stationCode).Any() ? stations.Where(x => x.stationCode == k.stationCode).First() : (station)null;
                if (s != null) {
                    if (s.hkpi == null)
                        s.hkpi = new List<hkpi>();
                    s.hkpi.Add(k);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    DataTable dt = connection.ExecuteResultSet("Select count(*) From ETA05PVMONITORHOURKPI where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime", prms);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    prms.Add("@radiation_intensity", k.dataItemMap._radiation_intensity, DbType.Double);
                    prms.Add("@theory_power", k.dataItemMap._theory_power, DbType.Double);
                    prms.Add("@inverter_power", k.dataItemMap._inverter_power, DbType.Double);
                    prms.Add("@ongrid_power", k.dataItemMap._ongrid_power, DbType.Double);
                    prms.Add("@power_profit", k.dataItemMap._power_profit, DbType.Double);
                    if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) <= 0)
                    {
                        connection.ExecuteResultSet(@"Insert into ETA05PVMONITORHOURKPI(etA05ID, ETA05STATIONID, ETA05COLLECTDATETIME, eta05radiation_intensity, eta05theory_power, eta05inverter_power, eta05ongrid_power, eta05power_profit) 
                                                          values(NEWID(), @stationID, @collectionTime, @radiation_intensity, @theory_power, @inverter_power, @ongrid_power, @power_profit)", prms);
                    }
                }
            }
            #endregion

            #region Day KPIs
            List<dkpi> dkpis = getKpiStationDay(stations, collectionDatetime);
            foreach (dkpi k in dkpis)
            {
                station s = stations.Where(x => x.stationCode == k.stationCode).Any() ? stations.Where(x => x.stationCode == k.stationCode).First() : (station)null;
                if (s != null)
                {
                    if (s.dkpi == null)
                        s.dkpi = new List<dkpi>();
                    s.dkpi.Add(k);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    DataTable dt = connection.ExecuteResultSet("Select count(*) From ETA05PVMONITORDMYKPI where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = 0", prms);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    prms.Add("@use_power", k.dataItemMap._use_power, DbType.Double);
                    prms.Add("@radiation_intensity", k.dataItemMap._radiation_intensity, DbType.Double);
                    prms.Add("@reduction_total_co2", k.dataItemMap._reduction_total_co2, DbType.Double);
                    prms.Add("@reduction_total_coal", k.dataItemMap._reduction_total_coal, DbType.Double);
                    prms.Add("@theory_power", k.dataItemMap._theory_power, DbType.Double);
                    prms.Add("@inverter_power", k.dataItemMap._inverter_power, DbType.Double);
                    prms.Add("@ongrid_power", k.dataItemMap._ongrid_power, DbType.Double);
                    prms.Add("@power_profit", k.dataItemMap._power_profit, DbType.Double);
                    prms.Add("@installed_capacity", k.dataItemMap._installed_capacity, DbType.Double);
                    prms.Add("@perpower_ratio", k.dataItemMap._perpower_ratio, DbType.Double);
                    prms.Add("@reduction_total_tree", k.dataItemMap._reduction_total_tree, DbType.Int64);
                    prms.Add("@performance_ratio", k.dataItemMap._performance_ratio, DbType.Double);
                    prms.Add("@dmykpi", 0, DbType.Int64);
                    if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) <= 0)
                    {
                        connection.ExecuteResultSet(@"Insert into ETA05PVMONITORDMYKPI
                        (etA05ID, ETA05STATIONID, ETA05COLLECTDATETIME, etA05use_power, etA05radiation_intensity, etA05reduction_total_co2, etA05reduction_total_coal, etA05theory_power, etA05inverter_power, etA05ongrid_power
                        , etA05power_profit, etA05installed_capacity, etA05perpower_ratio, etA05reduction_total_tree, etA05performance_ratio, eta05dmykpi)
                        values(NEWID(), @stationID, @collectionTime, @use_power, @radiation_intensity, @reduction_total_co2, @reduction_total_coal, @theory_power, @inverter_power, @ongrid_power
                        , @power_profit, @installed_capacity, @perpower_ratio, @reduction_total_tree, @performance_ratio, @dmykpi)", prms);
                    }
                    else if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) > 0 && Math.Floor((DateTime.Now - k.collectDateTime).TotalDays) == 0)
                    {
                        connection.ExecuteResultSet(@"Update ETA05PVMONITORDMYKPI
                        set etA05use_power = @use_power, etA05radiation_intensity = @radiation_intensity, etA05reduction_total_co2 = @reduction_total_co2, etA05reduction_total_coal = @reduction_total_coal, etA05theory_power = @theory_power, etA05inverter_power = @inverter_power, etA05ongrid_power = @ongrid_power
                        , etA05power_profit = @power_profit, etA05installed_capacity = @installed_capacity, etA05perpower_ratio = @perpower_ratio, etA05reduction_total_tree = @reduction_total_tree, etA05performance_ratio = @performance_ratio, eta05dmykpi = @dmykpi
                        where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = @dmykpi", prms);
                    }
                }
            }
            #endregion

            #region month KPIs
            List<dkpi> mkpis = getKpiStationMonth(stations, collectionDatetime);
            foreach (dkpi k in mkpis)
            {
                station s = stations.Where(x => x.stationCode == k.stationCode).Any() ? stations.Where(x => x.stationCode == k.stationCode).First() : (station)null;
                if (s != null)
                {
                    if (s.mkpi == null)
                        s.mkpi = new List<dkpi>();
                    s.mkpi.Add(k);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    DataTable dt = connection.ExecuteResultSet("Select count(*) From ETA05PVMONITORDMYKPI where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = 1", prms);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    prms.Add("@use_power", k.dataItemMap._use_power, DbType.Double);
                    prms.Add("@radiation_intensity", k.dataItemMap._radiation_intensity, DbType.Double);
                    prms.Add("@reduction_total_co2", k.dataItemMap._reduction_total_co2, DbType.Double);
                    prms.Add("@reduction_total_coal", k.dataItemMap._reduction_total_coal, DbType.Double);
                    prms.Add("@theory_power", k.dataItemMap._theory_power, DbType.Double);
                    prms.Add("@inverter_power", k.dataItemMap._inverter_power, DbType.Double);
                    prms.Add("@ongrid_power", k.dataItemMap._ongrid_power, DbType.Double);
                    prms.Add("@power_profit", k.dataItemMap._power_profit, DbType.Double);
                    prms.Add("@installed_capacity", k.dataItemMap._installed_capacity, DbType.Double);
                    prms.Add("@perpower_ratio", k.dataItemMap._perpower_ratio, DbType.Double);
                    prms.Add("@reduction_total_tree", k.dataItemMap._reduction_total_tree, DbType.Int64);
                    prms.Add("@performance_ratio", k.dataItemMap._performance_ratio, DbType.Double);
                    prms.Add("@dmykpi", 1, DbType.Int64);
                    if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) <= 0)
                    {
                        connection.ExecuteResultSet(@"Insert into ETA05PVMONITORDMYKPI
                        (etA05ID, ETA05STATIONID, ETA05COLLECTDATETIME, etA05use_power, etA05radiation_intensity, etA05reduction_total_co2, etA05reduction_total_coal, etA05theory_power, etA05inverter_power, etA05ongrid_power
                        , etA05power_profit, etA05installed_capacity, etA05perpower_ratio, etA05reduction_total_tree, etA05performance_ratio, eta05dmykpi)
                        values(NEWID(), @stationID, @collectionTime, @use_power, @radiation_intensity, @reduction_total_co2, @reduction_total_coal, @theory_power, @inverter_power, @ongrid_power
                        , @power_profit, @installed_capacity, @perpower_ratio, @reduction_total_tree, @performance_ratio, @dmykpi)", prms);
                    }
                    else if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) > 0  && Math.Floor((decimal)((DateTime.Now.Month - k.collectDateTime.Month) + 12 * (DateTime.Now.Year - k.collectDateTime.Year))) == 0)
                    {
                        connection.ExecuteResultSet(@"Update ETA05PVMONITORDMYKPI
                        set etA05use_power = @use_power, etA05radiation_intensity = @radiation_intensity, etA05reduction_total_co2 = @reduction_total_co2, etA05reduction_total_coal = @reduction_total_coal, etA05theory_power = @theory_power, etA05inverter_power = @inverter_power, etA05ongrid_power = @ongrid_power
                        , etA05power_profit = @power_profit, etA05installed_capacity = @installed_capacity, etA05perpower_ratio = @perpower_ratio, etA05reduction_total_tree = @reduction_total_tree, etA05performance_ratio = @performance_ratio, eta05dmykpi = @dmykpi
                        where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = @dmykpi", prms);
                    }
                }
            }
            #endregion

            #region year KPIs
            List<dkpi> ykpis = getKpiStationYear(stations, collectionDatetime);
            foreach (dkpi k in ykpis)
            {
                station s = stations.Where(x => x.stationCode == k.stationCode).Any() ? stations.Where(x => x.stationCode == k.stationCode).First() : (station)null;
                if (s != null)
                {
                    if (s.ykpi == null)
                        s.ykpi = new List<dkpi>();
                    s.ykpi.Add(k);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    DataTable dt = connection.ExecuteResultSet("Select count(*) From ETA05PVMONITORDMYKPI where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = 2", prms);
                    prms = new slQueryParameters();
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    prms.Add("@collectionTime", k.collectDateTime, DbType.DateTime);
                    prms.Add("@use_power", k.dataItemMap._use_power, DbType.Double);
                    prms.Add("@radiation_intensity", k.dataItemMap._radiation_intensity, DbType.Double);
                    prms.Add("@reduction_total_co2", k.dataItemMap._reduction_total_co2, DbType.Double);
                    prms.Add("@reduction_total_coal", k.dataItemMap._reduction_total_coal, DbType.Double);
                    prms.Add("@theory_power", k.dataItemMap._theory_power, DbType.Double);
                    prms.Add("@inverter_power", k.dataItemMap._inverter_power, DbType.Double);
                    prms.Add("@ongrid_power", k.dataItemMap._ongrid_power, DbType.Double);
                    prms.Add("@power_profit", k.dataItemMap._power_profit, DbType.Double);
                    prms.Add("@installed_capacity", k.dataItemMap._installed_capacity, DbType.Double);
                    prms.Add("@perpower_ratio", k.dataItemMap._perpower_ratio, DbType.Double);
                    prms.Add("@reduction_total_tree", k.dataItemMap._reduction_total_tree, DbType.Int64);
                    prms.Add("@performance_ratio", k.dataItemMap._performance_ratio, DbType.Double);
                    prms.Add("@dmykpi", 2, DbType.Int64);
                    if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) <= 0)
                    {
                        connection.ExecuteResultSet(@"Insert into ETA05PVMONITORDMYKPI
                        (etA05ID, ETA05STATIONID, ETA05COLLECTDATETIME, etA05use_power, etA05radiation_intensity, etA05reduction_total_co2, etA05reduction_total_coal, etA05theory_power, etA05inverter_power, etA05ongrid_power
                        , etA05power_profit, etA05installed_capacity, etA05perpower_ratio, etA05reduction_total_tree, etA05performance_ratio, eta05dmykpi)
                        values(NEWID(), @stationID, @collectionTime, @use_power, @radiation_intensity, @reduction_total_co2, @reduction_total_coal, @theory_power, @inverter_power, @ongrid_power
                        , @power_profit, @installed_capacity, @perpower_ratio, @reduction_total_tree, @performance_ratio, @dmykpi)", prms);
                    }
                    else if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) && int.Parse(dt.Rows[0][0].ToString()) > 0 && Math.Floor((decimal)(DateTime.Now.Year - k.collectDateTime.Year)) == 0)
                    {
                        connection.ExecuteResultSet(@"Update ETA05PVMONITORDMYKPI
                        set etA05use_power = @use_power, etA05radiation_intensity = @radiation_intensity, etA05reduction_total_co2 = @reduction_total_co2, etA05reduction_total_coal = @reduction_total_coal, etA05theory_power = @theory_power, etA05inverter_power = @inverter_power, etA05ongrid_power = @ongrid_power
                        , etA05power_profit = @power_profit, etA05installed_capacity = @installed_capacity, etA05perpower_ratio = @perpower_ratio, etA05reduction_total_tree = @reduction_total_tree, etA05performance_ratio = @performance_ratio, eta05dmykpi = @dmykpi
                        where ETA05STATIONID = @stationID and ETA05COLLECTDATETIME=@collectionTime and eta05dmykpi = @dmykpi", prms);
                    }
                }
            }
            #endregion

        }//postKPIs

        public void postKPIs()
        {
            List<station> stations = stationFactory();
            stations = updateStations(stations);
            postKPIs(stations);
        }//postKPIs

        public List<station> updateStations(List<station> stations)
        {
            List<station> response = stations;
            foreach (station s in stations)
            {
                slQueryParameters prms = new slQueryParameters("@stationCode", s.stationCode);
                DataTable dt = connection.ExecuteResultSet("Select * From etA05PvMonitorStations where etA05StationCode = @stationCode", prms);
                if (dt.Rows.Count <= 0)
                {
                    prms = new slQueryParameters();
                    prms.Add("@etA05StationCode", s.stationCode, DbType.String);
                    prms.Add("@etA05StationName", s.stationName, DbType.String);
                    prms.Add("@etA05StationAddr", s.stationAddr, DbType.String);
                    prms.Add("@etA05Capacity", s.capacity, DbType.Double);
                    prms.Add("@etA05BuildState", s.buildState, DbType.Int16);
                    prms.Add("@etA05CombineType", s.combineType, DbType.Int16);
                    prms.Add("@etA05AidType", s.aidType, DbType.Int16);
                    prms.Add("@etA05StationLinkman", s.stationLinkman, DbType.String);
                    prms.Add("@etA05LinkmanPho", s.linkmanPho, DbType.String);
                    dt = connection.ExecuteResultSet(@" declare  @ID uniqueidentifier  = NEWID(); 
                                                        Insert  into etA05PvMonitorStations (etA05ID, etA05StationCode, etA05StationName, etA05StationAddr, etA05Capacity, etA05BuildState, etA05CombineType, etA05AidType, etA05StationLinkman, etA05LinkmanPho) OUTPUT @ID  
                                                        values(@ID, @etA05StationCode, @etA05StationName, @etA05StationAddr, @etA05Capacity, @etA05BuildState, @etA05CombineType, @etA05AidType, @etA05StationLinkman, @etA05LinkmanPho)", prms);
                    if (dt.Rows.Count > 0)
                        s.stationID = dt.Rows[0][0] != null && !string.IsNullOrEmpty(dt.Rows[0][0].ToString()) ? Guid.Parse(dt.Rows[0][0].ToString()) : Guid.Empty;
                }
                else if (dt.Rows.Count > 0)
                {
                    s.stationID = dt.Rows[0]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[0]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[0]["etA05ID"].ToString()) : Guid.Empty;
                }
            }
            return response;
        }//updateStations

        public List<station> updateStations()
        {
            List<station> stations = stationFactory();
            stations = updateStations(stations);
            return stations;
        }//updateStations

        public List<station> stationFactory()
        {
            request r = new request("{}", "getStationList", authenticator);
            JArray rdt = r.reqResponse.data as JArray;
            List<station> collection = new List<station>();
            if (!r.reqResponse.success || r.reqResponse.failCode == 20007)
                return collection;
            foreach (JObject obj in rdt)
            {
                station s = JsonConvert.DeserializeObject<station>(obj.ToString());
                s.authenticator = authenticator;
                collection.Add(s);
            }
            return collection;
        }//stationFactory

        public List<dkpi> getKpiStationYear(List<station> stations, DateTime d)
        {
            ///
            /// ---- Interface for Yearly Plant Data ----
            /// 
            /// This interface is used to obtain the yearly statistics of the plant. You can query the monthly
            /// statistics by plant ID and time range.You can query yearly statistics of 25 years for a
            /// maximum of 100 plants at a time.
            ///
            List<dkpi> resp = new List<dkpi>();
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(getKpiStationYear100(stations.GetRange(lowerLimit, upperLimit), d));
            }
            return resp;
        }//getKpiStationYear

        public List<dkpi> getKpiStationMonth(List<station> stations, DateTime d)
        {
            ///
            /// ---- Interface for Monthly Plant Data ----
            /// 
            /// This interface is used to obtain the monthly statistics of plants. You can query the monthly
            /// statistics by plant ID and time range.You can query monthly statistics of a year for a
            /// maximum of 100 plants at a time.
            ///
            List<dkpi> resp = new List<dkpi>();
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(getKpiStationMonth100(stations.GetRange(lowerLimit, upperLimit), d));
            }
            return resp;
        }//getKpiStationMonth

        public List<dkpi> getKpiStationDay(List<station> stations, DateTime d)
        {
            ///
            /// ---- Interface for Daily Plant Data ----
            /// 
            /// This interface is used to obtain the daily statistics of plants. You can query the daily statistics
            /// by plant ID and time range. You can query daily statistics of a month for a maximum of 100
            /// plants at a time.
            ///
            List<dkpi> resp = new List<dkpi>();
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(getKpiStationDay100(stations.GetRange(lowerLimit, upperLimit), d));
            }
            return resp;
        }//getKpiStationDay

        public List<hkpi> getKpiStationHour(List<station> stations, DateTime d)
        {
            ///
            /// ---- Interface for Hourly Plant Data ----
            /// 
            /// This interface is used to obtain the hourly statistics of plants. You can query the hourly
            /// statistics by plant ID and time range.You can query hourly statistics of a day for a maximum
            /// of 100 plants at a time.
            ///
            List<hkpi> resp = new List<hkpi>();
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                resp.AddRange(getKpiStationHour100(stations.GetRange(lowerLimit, upperLimit), d));
            }
            return resp;
        }//getKpiStationHour

        public void getStationRealKpi(List<station> stations)
        {
            ///
            /// ---- Interface for Real-time Plant Data ----
            /// 
            /// This interface is used to obtain the real-time statistics of plants. You can query statistics by
            /// plant ID. A maximum of 100 plants can be queried at a time
            ///
            for (int i = 0; i < Math.Ceiling((double)((double)stations.Count / (double)100)); i++)
            {
                int lowerLimit = i * 100;
                int upperLimit = (stations.Count - (i * 100)) < 100 ? (stations.Count - (i * 100)) : 100;
                getStationRealKpiPer100(stations.GetRange(lowerLimit, upperLimit));
            }
        }//getStationRealKpi

        public void getStationRealKpiPer100(List<station> stations)
        {
            string body = string.Empty;
            foreach (station s in stations)
            {
                body += s.stationCode + ",";
            }
            body = "{\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"}";
            request r = new request(body, "getStationRealKpi", authenticator);
            if (!r.reqResponse.success)
            {
                foreach (station s in stations)
                {
                    kpi k = new kpi();
                    k.errorMessage = r.reqResponse.data.ToString();
                    s.realkpi = k;
                }
                return;
            }
            JArray rdt = (r.reqResponse.data as JArray);
            foreach (JObject obj in rdt)
            {
                kpi k = JsonConvert.DeserializeObject<kpi>(obj.ToString());
                if (k != null && stations.Where(x => x.stationCode == k.stationCode).Any())
                    stations.Where(x => x.stationCode == k.stationCode).First().realkpi = k;

            }
        }//getStationRealKpiPer100

        public List<hkpi> getKpiStationHour100(List<station> stations, DateTime d)
        {
            string body = string.Empty;
            List<hkpi> resp = new List<hkpi>();
            foreach (station s in stations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            long collectTime = Convert.ToInt64((d - DateTime.Parse("1/1/1970")).TotalMilliseconds);
            body = "{" + body + ", \"collectTime\":\"" + collectTime + "\"}";
            request r = new request(body, "getKpiStationHour", authenticator);
            if (!r.reqResponse.success)
            {
                foreach (station s in stations)
                {
                    hkpi k = new hkpi();
                    k.errorMessage = r.reqResponse.data.ToString();
                    k.stationCode = s.stationCode;
                    k.collectTime = collectTime;
                    resp.Add(k);
                }
                return resp;
            }
            JArray rdt = (r.reqResponse.data as JArray);
            long currentTime = long.Parse((r.reqResponse.@params as JObject).Properties().Where(x => x.Name == "currentTime").FirstOrDefault().Value.ToString());
            foreach (JObject obj in rdt)
            {
                hkpi k = JsonConvert.DeserializeObject<hkpi>(obj.ToString());
                double h = Math.Round(((double)collectTime - (double)currentTime) / 1000 / 60 / 60);
                k.collectDateTime = k.collectDateTime.AddHours(h);
                k.collectTime = k.collectTime + ((long)h * 1000 * 60 * 60);
                if (k != null)
                    resp.Add(k);
            }
            return resp;
        }//getKpiStationHour100

        public List<dkpi> getKpiStationDay100(List<station> stations, DateTime d)
        {
            string body = string.Empty;
            List<dkpi> resp = new List<dkpi>();
            foreach (station s in stations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            long collectTime = Convert.ToInt64((d - DateTime.Parse("1/1/1970")).TotalMilliseconds);
            body = "{" + body + ", \"collectTime\":\"" + collectTime + "\"}";
            request r = new request(body, "getKpiStationDay", authenticator);
            if (!r.reqResponse.success)
            {
                foreach (station s in stations)
                {
                    dkpi k = new dkpi();
                    k.errorMessage = r.reqResponse.data.ToString();
                    k.stationCode = s.stationCode;
                    k.collectTime = collectTime;
                    resp.Add(k);
                }
                return resp;
            }
            JArray rdt = (r.reqResponse.data as JArray);
            long currentTime = long.Parse((r.reqResponse.@params as JObject).Properties().Where(x => x.Name == "currentTime").FirstOrDefault().Value.ToString());
            foreach (JObject obj in rdt)
            {
                dkpi k = JsonConvert.DeserializeObject<dkpi>(obj.ToString());
                double h = Math.Round(((double)collectTime - (double)currentTime) / 1000 / 60 / 60);
                k.collectDateTime = k.collectDateTime.AddHours(h);
                k.collectTime = k.collectTime + ((long)h*1000*60*60);
                if (k != null)
                    resp.Add(k);
            }
            return resp;
        }//getKpiStationDay100

        public List<dkpi> getKpiStationMonth100(List<station> stations, DateTime d)
        {
            string body = string.Empty;
            List<dkpi> resp = new List<dkpi>();
            foreach (station s in stations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            long collectTime = Convert.ToInt64((d - DateTime.Parse("1/1/1970")).TotalMilliseconds);
            body = "{" + body + ", \"collectTime\":\"" + collectTime + "\"}";
            request r = new request(body, "getKpiStationMonth", authenticator);
            if (!r.reqResponse.success)
            {
                foreach (station s in stations)
                {
                    dkpi k = new dkpi();
                    k.errorMessage = r.reqResponse.data.ToString();
                    k.stationCode = s.stationCode;
                    k.collectTime = collectTime;
                    resp.Add(k);
                }
                return resp;
            }
            JArray rdt = (r.reqResponse.data as JArray);
            long currentTime = long.Parse((r.reqResponse.@params as JObject).Properties().Where(x => x.Name == "currentTime").FirstOrDefault().Value.ToString());
            foreach (JObject obj in rdt)
            {
                dkpi k = JsonConvert.DeserializeObject<dkpi>(obj.ToString());
                double h = Math.Round(((double)collectTime - (double)currentTime) / 1000 / 60 / 60);
                k.collectDateTime = k.collectDateTime.AddHours(h);
                k.collectTime = k.collectTime + ((long)h * 1000 * 60 * 60);
                if (k != null)
                    resp.Add(k);
            }
            return resp;
        }//getKpiStationMonth100

        public List<dkpi> getKpiStationYear100(List<station> stations, DateTime d)
        {
            string body = string.Empty;
            List<dkpi> resp = new List<dkpi>();
            foreach (station s in stations)
            {
                body += s.stationCode + ",";
            }
            body = "\"stationCodes\":\"" + body.Substring(0, body.LastIndexOf(',')) + "\"";
            long collectTime = Convert.ToInt64((d - DateTime.Parse("1/1/1970")).TotalMilliseconds);
            body = "{" + body + ", \"collectTime\":\"" + collectTime + "\"}";
            request r = new request(body, "getKpiStationYear", authenticator);
            if (!r.reqResponse.success)
            {
                foreach (station s in stations)
                {
                    dkpi k = new dkpi();
                    k.errorMessage = r.reqResponse.data.ToString();
                    k.stationCode = s.stationCode;
                    k.collectTime = collectTime;
                    resp.Add(k);
                }
                return resp;
            }
            JArray rdt = (r.reqResponse.data as JArray);
            long currentTime = long.Parse((r.reqResponse.@params as JObject).Properties().Where(x => x.Name == "currentTime").FirstOrDefault().Value.ToString());
            foreach (JObject obj in rdt)
            {
                dkpi k = JsonConvert.DeserializeObject<dkpi>(obj.ToString());
                double h = Math.Round(((double)collectTime - (double)currentTime) / 1000 / 60 / 60);
                k.collectDateTime = k.collectDateTime.AddHours(h);
                k.collectTime = k.collectTime + ((long)h * 1000 * 60 * 60);
                if (k != null)
                    resp.Add(k);
            }
            return resp;
        }//getKpiStationYear100
    }
}
