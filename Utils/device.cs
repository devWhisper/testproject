﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;
using SLnet.Base.DbUtils;

namespace HuaweiAPI.Utils
{
    public class device
    {

        public string id { get; set; }//Device ID
        public Guid dbid { get; set; }//Database ID
        public string devName { get; set; }//Device name
        public string stationCode { get; set; }//Plant name
        public string esnCode { get; set; }//Device SN
        /// <summary> 
        /// devTypeId explaination
        /// </summary>
        /// 
        /// Device type ID
        ///1 Smart String Inverter
        ///2 SmartLogger
        ///3 String
        ///6 Bay
        ///7 Busbar
        ///8 Transformer
        ///9 Transformer meter
        ///10 EMI
        ///11 AC combiner box
        ///13 DPU
        ///14 Central Inverter
        ///15 DC combiner box
        ///16 General device
        ///17 Grid meter
        ///18 Step-up station
        ///19 Factory-used energy generation area meter
        ///20 Solar power forecasting system
        ///21 Factory-used energy non-generation area meter
        ///22 PID
        ///23 Virtual device of plant monitoring system
        ///24 Power quality device
        ///25 Step-up transformer
        ///26 Photovoltaic grid-connection cabinet
        ///27 Photovoltaic grid-connection panel
        ///37 Pinnet SmartLogger
        ///38 Smart Energy Center
        ///39 Battery
        ///40 Smart Backup Box
        ///45 MBUS
        ///46 Optimizer
        ///47 Power Sensor
        ///52 SAJ data logger
        ///53 High voltage bay of the main transformer
        ///54 Main transformer
        ///55 Low voltage bay of the main transformer
        ///56 Bus bay
        ///57 Line bay
        ///58 Plant transformer bay
        ///59 SVC/SVG bay
        ///60 Bus tie/section bay
        ///61 Plant power supply device
        ///62 Dongle
        ///63 Distributed SmartLogger
        ///70 Safety box
        public int devTypeId
        {
            get
            {
                return _devTypeId;
            }
            set
            {
                _devTypeId = value;
                switch (value)
                {
                    case 1:
                        sdevTypeId = "Smart String Inverter";
                        break;
                    case 2:
                        sdevTypeId = "SmartLogger";
                        break;
                    case 3:
                        sdevTypeId = "String";
                        break;
                    case 6:
                        sdevTypeId = "Bay";
                        break;
                    case 7:
                        sdevTypeId = "Busbar";
                        break;
                    case 8:
                        sdevTypeId = "Transformer";
                        break;
                    case 9:
                        sdevTypeId = "Transformer meter";
                        break;
                    case 10:
                        sdevTypeId = "EMI";
                        break;
                    case 11:
                        sdevTypeId = "AC combiner box";
                        break;
                    case 13:
                        sdevTypeId = "DPU";
                        break;
                    case 14:
                        sdevTypeId = "Central Inverter";
                        break;
                    case 15:
                        sdevTypeId = "DC combiner box";
                        break;

                    case 16:
                        sdevTypeId = "General device";
                        break;
                    case 17:
                        sdevTypeId = "Grid meter";
                        break;
                    case 18:
                        sdevTypeId = "Step-up station";
                        break;
                    case 19:
                        sdevTypeId = "Factory-used energy generation area meter";
                        break;
                    case 20:
                        sdevTypeId = "Solar power forecasting system";
                        break;
                    case 21:
                        sdevTypeId = "Factory-used energy non-generation area meter";
                        break;
                    case 22:
                        sdevTypeId = "PID";
                        break;
                    case 23:
                        sdevTypeId = "Virtual device of plant monitoring system";
                        break;
                    case 24:
                        sdevTypeId = "Power quality device";
                        break;
                    case 25:
                        sdevTypeId = "Step-up transformer";
                        break;
                    case 26:
                        sdevTypeId = "Photovoltaic grid-connection cabinet";
                        break;
                    case 27:
                        sdevTypeId = "Photovoltaic grid-connection panel";
                        break;
                    case 37:
                        sdevTypeId = "Pinnet SmartLogger";
                        break;
                    case 38:
                        sdevTypeId = "Smart Energy Center";
                        break;
                    case 39:
                        sdevTypeId = "Battery";
                        break;
                    case 40:
                        sdevTypeId = "Smart Backup Box";
                        break;
                    case 45:
                        sdevTypeId = "MBUS";
                        break;
                    case 46:
                        sdevTypeId = "Optimizer";
                        break;
                    case 47:
                        sdevTypeId = "Power Sensor";
                        break;
                    case 52:
                        sdevTypeId = "SAJ data logger";
                        break;
                    case 53:
                        sdevTypeId = "High voltage bay of the main transformer";
                        break;
                    case 54:
                        sdevTypeId = "Main transformer";
                        break;
                    case 55:
                        sdevTypeId = "Low voltage bay of the main transformer";
                        break;
                    case 56:
                        sdevTypeId = "Bus bay";
                        break;
                    case 57:
                        sdevTypeId = "Line bay";
                        break;
                    case 58:
                        sdevTypeId = "Plant transformer bay";
                        break;
                    case 59:
                        sdevTypeId = "SVC/SVG bay";
                        break;
                    case 60:
                        sdevTypeId = "Bus tie/section bay";
                        break;
                    case 61:
                        sdevTypeId = "Plant power supply device";
                        break;
                    case 62:
                        sdevTypeId = "Dongle";
                        break;
                    case 63:
                        sdevTypeId = "Distributed SmartLogger";
                        break;
                    case 70:
                        sdevTypeId = "Safety box";
                        break;
                    default:
                        sdevTypeId = string.Empty;
                        break;
                }
            }
        }
        public int _devTypeId { get; set; }
        public string sdevTypeId { get; set; }
        public string softwareVersion { get; set; }//Software version
        public string invType { get; set; }//Inverter model (only applicable to the inverter)
        public string longitude { get; set; }
        public string latitude { get; set; }

        public device()
        {

        }//device

        public List<deviceAlarm> deviceAlarmFactoryLst(Connection connection)
        {
            List<deviceAlarm> dummyLst = new List<deviceAlarm>();
            string sql = @" SELECT *
                            FROM ETA05PVMONITORDEVICEALARM ALRM
                            inner join ETA05PVMONITORDEVICES DEV on DEV.ETA05ESNCODE = ALRM.ETA05ESNCODE
                            where 1=1
                            and ALRM.ETA05ESNCODE = @SN";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@SN", esnCode, DbType.String);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    deviceAlarm da = new deviceAlarm();
                    da.dbid = dt.Rows[i]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[i]["etA05ID"].ToString()) : Guid.Empty;
                    da.stationCode = dt.Rows[i]["eta05stationCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05stationCode"].ToString()) ? dt.Rows[i]["eta05stationCode"].ToString() : string.Empty;
                    da.alarmName = dt.Rows[i]["eta05AlarmName"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05AlarmName"].ToString()) ? dt.Rows[i]["eta05AlarmName"].ToString() : string.Empty;
                    da.devName = dt.Rows[i]["eta05DevName"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05DevName"].ToString()) ? dt.Rows[i]["eta05DevName"].ToString() : string.Empty;
                    da.repairSuggestion = dt.Rows[i]["eta05repairSuggestion"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05repairSuggestion"].ToString()) ? dt.Rows[i]["eta05repairSuggestion"].ToString() : string.Empty;
                    da.esnCode = dt.Rows[i]["eta05esnCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05esnCode"].ToString()) ? dt.Rows[i]["eta05esnCode"].ToString() : string.Empty;
                    //d.devTypeId = dt.Rows[i]["etA05devTypeId"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05devTypeId"].ToString()) ? int.Parse(dt.Rows[i]["etA05devTypeId"].ToString()) : -1;
                    //d.esnCode = dt.Rows[i]["etA05esnCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05esnCode"].ToString()) ? dt.Rows[i]["etA05esnCode"].ToString() : string.Empty;
                    //d.id = dt.Rows[i]["etA05apiDevid"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05apiDevid"].ToString()) ? dt.Rows[i]["etA05apiDevid"].ToString() : string.Empty;
                    //d.invType = dt.Rows[i]["eta05invType"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05invType"].ToString()) ? dt.Rows[i]["eta05invType"].ToString() : string.Empty;
                    //d.latitude = dt.Rows[i]["eta05latitude"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05latitude"].ToString()) ? dt.Rows[i]["eta05latitude"].ToString() : string.Empty;
                    //d.longitude = dt.Rows[i]["eta05longitude"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05longitude"].ToString()) ? dt.Rows[i]["eta05longitude"].ToString() : string.Empty;
                    //d.softwareVersion = dt.Rows[i]["etA05softwareVersion"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05softwareVersion"].ToString()) ? dt.Rows[i]["etA05softwareVersion"].ToString() : string.Empty;
                    //d.stationCode = dt.Rows[i]["etA05stationCode"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05stationCode"].ToString()) ? dt.Rows[i]["etA05stationCode"].ToString() : string.Empty;
                    dummyLst.Add(da);
                }
            }
            return dummyLst;
        }//devFactoryLst
    }

    public class deviceAlarm
    {
        public Guid dbid { get; set; }//Database ID
        public string stationCode { get; set; }
        public string alarmName { get; set; }
        public string devName { get; set; }
        public string repairSuggestion { get; set; }
        public string esnCode { get; set; }
        public int devTypeId { get; set; }
        public int causeId { get; set; }
        public string alarmCause { get; set; }
        public int alarmType { get; set; }
        public DateTime raiseDateTime { get; set; }
        public long _raiseTime { get; set; }
        public long raiseTime
        {
            get { return _raiseTime; }
            set
            {
                _raiseTime = value;
                raiseDateTime = DateTime.Parse("1/1/1970").AddMilliseconds(value);
            }
        }
        public int alarmId { get; set; }
        public string stationName { get; set; }
        public int lev { get; set; }
        public int status { get; set; }
    }
}
