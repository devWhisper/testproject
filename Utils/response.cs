﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HuaweiAPI.Utils
{
    class response
    {
        public string buildCode { get; set; }
        public object data { get; set; }
        public int failCode { get; set; }
        public string message { get; set; }
        public object @params { get; set; }
        public bool success { get; set; }

        public response(){}

        public void responseFactory(string response)
        {
            response deserializedAuthResponse = JsonConvert.DeserializeObject<response>(response);
            buildCode = deserializedAuthResponse.buildCode;
            data = deserializedAuthResponse.data;
            failCode = deserializedAuthResponse.failCode;
            message = deserializedAuthResponse.message;
            @params = deserializedAuthResponse.@params;
            success = deserializedAuthResponse.success;
        }//responseFactory
    }
}
