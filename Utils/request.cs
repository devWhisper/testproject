﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Security;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using spc = Microsoft.SharePoint.Client;
using System.IO;
using System.Net;
using HuaweiAPI.Utils;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace HuaweiAPI.Utils
{
    class request
    {
        public string body { get; set; }
        public response reqResponse { get; set; }
        public string url { get; set; }

        public request() { }

        public request(string body, string url, auth a)
        {
            this.body = body;
            this.url = a.url + url;
            makeRequest(a);
        }

        public void makeRequest(auth a)
        {
            HttpWebRequest downloadRequest = (HttpWebRequest)WebRequest.Create(url);
            downloadRequest.CookieContainer = a.cookieContainer;
            downloadRequest.ContentType = "application/json";
            downloadRequest.Method = "POST";
            downloadRequest.Headers.Add("XSRF-TOKEN", a.XSRFTOKEN);
            using (var streamWriter = new StreamWriter(downloadRequest.GetRequestStream()))
            {
                streamWriter.Write(body);
            }
            try
            {
                var httpResponse = (HttpWebResponse)downloadRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    reqResponse = new response();
                    reqResponse.responseFactory(result);
                    if (reqResponse.failCode > 0)
                    {
                        //write to log table
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }//makeRequest
    }
}
