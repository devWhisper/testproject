﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HuaweiAPI.Utils;
using SLnet.Base.DbUtils;
using System.Security;
using HuaweiAPI.Forms;

namespace HuaweiAPI.Forms
{
    public partial class stationKPIs : Base
    {
        enum times
        {
            Ώρα,
            Ημέρα,
            Μήνα, 
            Έτος
        }

        public station currentItem = new station();
        public Connection connection = (Connection)null;
        public stationKPIs()
        {
            InitializeComponent();
            loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }//stationKPIs

        public void setStationData()
        {
            ctrlProjectCode.Text = currentItem.stationName;
            ctrlProjectDesc.Text = currentItem.projectName;
            ctrlProjectCapacity.Text = currentItem.capacity.ToString("n2") + " KW";
            ctrlProjectAddress.Text = currentItem.stationAddr;
            ctrlDayPower.Text = currentItem.realkpi.dataItemMap.day_power.ToString("n0") + " KWh";
            ctrlMonthPower.Text = (currentItem.realkpi.dataItemMap.month_power / 1000).ToString("n2") + " MWh";
            ctrlTotalPower.Text = (currentItem.realkpi.dataItemMap.total_power / 1000).ToString("n2") + " MWh";
            ctrlHealthyStatus.Text = currentItem.realkpi.dataItemMap.sreal_health_state;

            #region set times spans
            List<timeObj> t = new List<timeObj>();
            for (int i = 0; i < 4; i++)
            {
                timeObj to = new timeObj();
                to.value = i;
                to.desc = ((times)i).ToString();
                t.Add(to);
            }
            bsTimes.DataSource = typeof(timeObj);
            bsTimes.DataSource = t;
            #endregion

        }//setStationData

        public class timeObj
        {
            public int value { get; set; }
            public String desc { get; set; }
        }//timeObj

        private void ctrlTimeChooser_EditValueChanged(object sender, EventArgs e)
        {
            switch (ctrlTimeChooser.EditValue)
            {
                case 0:
                    loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    setHourlyData();
                    break;
                case 1:
                    loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    setDayilyData();
                    break;
                case 2:
                    loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    setMonthlyData();
                    break;
                case 3:
                    loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    setYearlyData();
                    break;
                default:
                    loHourlyData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    loDMYData.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    List<dKpiDataItem> dLst = new List<dKpiDataItem>();
                    bsDMYData.DataSource = dLst;
                    break;
            }
        }//ctrlTimeChooser_EditValueChanged

        private void setYearlyData()
        {
            List<dKpiDataItem> dLst = new List<dKpiDataItem>();
            string sql = @" select * from ETA05PVMONITORDMYKPI
                            where 1 = 1 
                            and ETA05DMYKPI = 2
                            and ETA05STATIONID = @stationID
                            order by ETA05COLLECTDATETIME desc";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@stationID", currentItem.stationID, DbType.Guid);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dKpiDataItem d = new dKpiDataItem();
                    d._inverter_power = dt.Rows[i]["ETA05inverter_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05inverter_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05inverter_power"].ToString()) : 0;
                    d._ongrid_power = dt.Rows[i]["ETA05ongrid_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05ongrid_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05ongrid_power"].ToString()) : 0;
                    d._power_profit = dt.Rows[i]["ETA05power_profit"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05power_profit"].ToString()) ? double.Parse(dt.Rows[i]["ETA05power_profit"].ToString()) : 0;
                    d._radiation_intensity = dt.Rows[i]["ETA05radiation_intensity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05radiation_intensity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05radiation_intensity"].ToString()) : 0;
                    d._theory_power = dt.Rows[i]["ETA05theory_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05theory_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05theory_power"].ToString()) : 0;
                    d.collectTime = dt.Rows[i]["ETA05collectDateTime"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05collectDateTime"].ToString()) ? DateTime.Parse(dt.Rows[i]["ETA05collectDateTime"].ToString()) : DateTime.Parse("1/1/1970");
                    d._installed_capacity = dt.Rows[i]["ETA05installed_capacity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05installed_capacity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05installed_capacity"].ToString()) : 0;
                    d._performance_ratio = dt.Rows[i]["ETA05performance_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05performance_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05performance_ratio"].ToString()) : 0;
                    d._perpower_ratio = dt.Rows[i]["ETA05perpower_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05perpower_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05perpower_ratio"].ToString()) : 0;
                    d._reduction_total_co2 = dt.Rows[i]["ETA05reduction_total_co2"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) : 0;
                    d._reduction_total_coal = dt.Rows[i]["ETA05reduction_total_coal"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) : 0;
                    d._reduction_total_tree = dt.Rows[i]["ETA05reduction_total_tree"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) ? int.Parse(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) : 0;
                    d._use_power = dt.Rows[i]["ETA05use_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05use_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05use_power"].ToString()) : 0;
                    dLst.Add(d);
                }
            }
            colDMYCollectDateTime.DisplayFormat.FormatString = "yyyy";
            bsDMYData.DataSource = dLst;
        }//setYearlyData

        private void setMonthlyData()
        {
            List<dKpiDataItem> dLst = new List<dKpiDataItem>();
            string sql = @" select * from ETA05PVMONITORDMYKPI
                            where 1 = 1 
                            and ETA05DMYKPI = 1
                            and ETA05STATIONID = @stationID
                            order by ETA05COLLECTDATETIME desc";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@stationID", currentItem.stationID, DbType.Guid);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dKpiDataItem d = new dKpiDataItem();
                    d._inverter_power = dt.Rows[i]["ETA05inverter_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05inverter_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05inverter_power"].ToString()) : 0;
                    d._ongrid_power = dt.Rows[i]["ETA05ongrid_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05ongrid_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05ongrid_power"].ToString()) : 0;
                    d._power_profit = dt.Rows[i]["ETA05power_profit"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05power_profit"].ToString()) ? double.Parse(dt.Rows[i]["ETA05power_profit"].ToString()) : 0;
                    d._radiation_intensity = dt.Rows[i]["ETA05radiation_intensity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05radiation_intensity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05radiation_intensity"].ToString()) : 0;
                    d._theory_power = dt.Rows[i]["ETA05theory_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05theory_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05theory_power"].ToString()) : 0;
                    d.collectTime = dt.Rows[i]["ETA05collectDateTime"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05collectDateTime"].ToString()) ? DateTime.Parse(dt.Rows[i]["ETA05collectDateTime"].ToString()) : DateTime.Parse("1/1/1970");
                    d._installed_capacity = dt.Rows[i]["ETA05installed_capacity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05installed_capacity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05installed_capacity"].ToString()) : 0;
                    d._performance_ratio = dt.Rows[i]["ETA05performance_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05performance_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05performance_ratio"].ToString()) : 0;
                    d._perpower_ratio = dt.Rows[i]["ETA05perpower_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05perpower_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05perpower_ratio"].ToString()) : 0;
                    d._reduction_total_co2 = dt.Rows[i]["ETA05reduction_total_co2"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) : 0;
                    d._reduction_total_coal = dt.Rows[i]["ETA05reduction_total_coal"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) : 0;
                    d._reduction_total_tree = dt.Rows[i]["ETA05reduction_total_tree"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) ? int.Parse(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) : 0;
                    d._use_power = dt.Rows[i]["ETA05use_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05use_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05use_power"].ToString()) : 0;
                    dLst.Add(d);
                }
            }
            colDMYCollectDateTime.DisplayFormat.FormatString = "MM/yyyy";
            bsDMYData.DataSource = dLst;
        }//setMonthlyData

        private void setDayilyData() 
        {
            List<dKpiDataItem> dLst = new List<dKpiDataItem>();
            string sql = @" select * from ETA05PVMONITORDMYKPI
                            where 1 = 1 
                            and ETA05DMYKPI = 0
                            and ETA05STATIONID = @stationID
                            order by ETA05COLLECTDATETIME desc";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@stationID", currentItem.stationID, DbType.Guid);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dKpiDataItem d = new dKpiDataItem();
                    d._inverter_power = dt.Rows[i]["ETA05inverter_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05inverter_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05inverter_power"].ToString()) : 0;
                    d._ongrid_power = dt.Rows[i]["ETA05ongrid_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05ongrid_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05ongrid_power"].ToString()) : 0;
                    d._power_profit = dt.Rows[i]["ETA05power_profit"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05power_profit"].ToString()) ? double.Parse(dt.Rows[i]["ETA05power_profit"].ToString()) : 0;
                    d._radiation_intensity = dt.Rows[i]["ETA05radiation_intensity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05radiation_intensity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05radiation_intensity"].ToString()) : 0;
                    d._theory_power = dt.Rows[i]["ETA05theory_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05theory_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05theory_power"].ToString()) : 0;
                    d.collectTime = dt.Rows[i]["ETA05collectDateTime"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05collectDateTime"].ToString()) ? DateTime.Parse(dt.Rows[i]["ETA05collectDateTime"].ToString()) : DateTime.Parse("1/1/1970");
                    d._installed_capacity = dt.Rows[i]["ETA05installed_capacity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05installed_capacity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05installed_capacity"].ToString()) : 0;
                    d._performance_ratio = dt.Rows[i]["ETA05performance_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05performance_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05performance_ratio"].ToString()) : 0;
                    d._perpower_ratio = dt.Rows[i]["ETA05perpower_ratio"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05perpower_ratio"].ToString()) ? double.Parse(dt.Rows[i]["ETA05perpower_ratio"].ToString()) : 0;
                    d._reduction_total_co2 = dt.Rows[i]["ETA05reduction_total_co2"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_co2"].ToString()) : 0;
                    d._reduction_total_coal = dt.Rows[i]["ETA05reduction_total_coal"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) ? double.Parse(dt.Rows[i]["ETA05reduction_total_coal"].ToString()) : 0;
                    d._reduction_total_tree = dt.Rows[i]["ETA05reduction_total_tree"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) ? int.Parse(dt.Rows[i]["ETA05reduction_total_tree"].ToString()) : 0;
                    d._use_power = dt.Rows[i]["ETA05use_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05use_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05use_power"].ToString()) : 0;
                    dLst.Add(d);
                }
            }
            colDMYCollectDateTime.DisplayFormat.FormatString = "dd/MM/yyyy";
            bsDMYData.DataSource = dLst;
        }//setDayilyData

        private void setHourlyData()
        {
            List<hKpiDataItem> hLst = new List<hKpiDataItem>();
            string sql = @" select * from ETA05PVMONITORHOURKPI
                            where 1 = 1 and ETA05STATIONID = @stationID
                            order by ETA05COLLECTDATETIME desc";
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@stationID", currentItem.stationID, DbType.Guid);
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    hKpiDataItem h = new hKpiDataItem();
                    h.inverter_power = dt.Rows[i]["ETA05inverter_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05inverter_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05inverter_power"].ToString()) : 0;
                    h.ongrid_power = dt.Rows[i]["ETA05ongrid_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05ongrid_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05ongrid_power"].ToString()) : 0;
                    h.power_profit = dt.Rows[i]["ETA05power_profit"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05power_profit"].ToString()) ? double.Parse(dt.Rows[i]["ETA05power_profit"].ToString()) : 0;
                    h.radiation_intensity = dt.Rows[i]["ETA05radiation_intensity"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05radiation_intensity"].ToString()) ? double.Parse(dt.Rows[i]["ETA05radiation_intensity"].ToString()) : 0;
                    h.theory_power = dt.Rows[i]["ETA05theory_power"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05theory_power"].ToString()) ? double.Parse(dt.Rows[i]["ETA05theory_power"].ToString()) : 0;
                    h.collectTime = dt.Rows[i]["ETA05collectDateTime"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05collectDateTime"].ToString()) ? DateTime.Parse(dt.Rows[i]["ETA05collectDateTime"].ToString()) : DateTime.Parse("1/1/1970");
                    hLst.Add(h);
                }
            }
            bsHourlyData.DataSource = hLst;
        }//setHourlyData

        private void grvHourlyData_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.Name == colIndex.Name)
            {
                e.DisplayText = (e.RowHandle + 1).ToString() + ".";
                e.CellValue = (e.RowHandle + 1);
            }
        }//grvHourlyData_CustomDrawCell

        private void grvDMYKPIData_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.Name == colDMYIndex.Name)
            {
                e.DisplayText = (e.RowHandle + 1).ToString() + ".";
                e.CellValue = (e.RowHandle + 1);
            }
        }//grvDMYKPIData_CustomDrawCell
    }
}
