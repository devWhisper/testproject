﻿
namespace HuaweiAPI.Forms
{
    partial class stationKPIs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.ctrlHealthyStatus = new DevExpress.XtraEditors.TextEdit();
            this.ctrlTotalPower = new DevExpress.XtraEditors.TextEdit();
            this.ctrlMonthPower = new DevExpress.XtraEditors.TextEdit();
            this.ctrlDayPower = new DevExpress.XtraEditors.TextEdit();
            this.ctrlMainTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageRealKPI = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.grcDMYKPIData = new DevExpress.XtraGrid.GridControl();
            this.bsDMYData = new System.Windows.Forms.BindingSource(this.components);
            this.grvDMYKPIData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDMYIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDMYCollectDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_use_power = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_radiation_intensity1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_reduction_total_co2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_reduction_total_coal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_theory_power1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ongrid_power1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_power_profit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_inverter_power1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_installed_capacity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_perpower_ratio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_reduction_total_tree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_performance_ratio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grcHourlyData = new DevExpress.XtraGrid.GridControl();
            this.bsHourlyData = new System.Windows.Forms.BindingSource(this.components);
            this.grvHourlyData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_radiation_intensity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_theory_power = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_inverter_power = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ongrid_power = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCollectTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_power_profit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ctrlTimeChooser = new DevExpress.XtraEditors.LookUpEdit();
            this.bsTimes = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.loTimeChooser = new DevExpress.XtraLayout.LayoutControlItem();
            this.loHourlyData = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.loDMYData = new DevExpress.XtraLayout.LayoutControlItem();
            this.ctrlProjectAddress = new DevExpress.XtraEditors.TextEdit();
            this.ctrlProjectDesc = new DevExpress.XtraEditors.TextEdit();
            this.ctrlProjectCapacity = new DevExpress.XtraEditors.TextEdit();
            this.ctrlProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.loProjectCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.loProjectDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.loProjectCapacity = new DevExpress.XtraLayout.LayoutControlItem();
            this.loProjectAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.loMainTabControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.loDayPower = new DevExpress.XtraLayout.LayoutControlItem();
            this.loMonthPower = new DevExpress.XtraLayout.LayoutControlItem();
            this.loTotalPower = new DevExpress.XtraLayout.LayoutControlItem();
            this.loHealthyStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlHealthyStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMonthPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlDayPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMainTabControl)).BeginInit();
            this.ctrlMainTabControl.SuspendLayout();
            this.tabPageRealKPI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDMYKPIData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDMYData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDMYKPIData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHourlyData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsHourlyData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHourlyData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTimeChooser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTimes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTimeChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loHourlyData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loDMYData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMainTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loDayPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMonthPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loHealthyStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(2254, 1098);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.ctrlHealthyStatus);
            this.layoutControl2.Controls.Add(this.ctrlTotalPower);
            this.layoutControl2.Controls.Add(this.ctrlMonthPower);
            this.layoutControl2.Controls.Add(this.ctrlDayPower);
            this.layoutControl2.Controls.Add(this.ctrlMainTabControl);
            this.layoutControl2.Controls.Add(this.ctrlProjectAddress);
            this.layoutControl2.Controls.Add(this.ctrlProjectDesc);
            this.layoutControl2.Controls.Add(this.ctrlProjectCapacity);
            this.layoutControl2.Controls.Add(this.ctrlProjectCode);
            this.layoutControl2.Location = new System.Drawing.Point(18, 18);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(2218, 1062);
            this.layoutControl2.TabIndex = 4;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // ctrlHealthyStatus
            // 
            this.ctrlHealthyStatus.Location = new System.Drawing.Point(1328, 114);
            this.ctrlHealthyStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlHealthyStatus.Name = "ctrlHealthyStatus";
            this.ctrlHealthyStatus.Properties.ReadOnly = true;
            this.ctrlHealthyStatus.Size = new System.Drawing.Size(872, 26);
            this.ctrlHealthyStatus.StyleController = this.layoutControl2;
            this.ctrlHealthyStatus.TabIndex = 12;
            // 
            // ctrlTotalPower
            // 
            this.ctrlTotalPower.Location = new System.Drawing.Point(1328, 82);
            this.ctrlTotalPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTotalPower.Name = "ctrlTotalPower";
            this.ctrlTotalPower.Properties.ReadOnly = true;
            this.ctrlTotalPower.Size = new System.Drawing.Size(872, 26);
            this.ctrlTotalPower.StyleController = this.layoutControl2;
            this.ctrlTotalPower.TabIndex = 11;
            // 
            // ctrlMonthPower
            // 
            this.ctrlMonthPower.Location = new System.Drawing.Point(1328, 50);
            this.ctrlMonthPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlMonthPower.Name = "ctrlMonthPower";
            this.ctrlMonthPower.Properties.ReadOnly = true;
            this.ctrlMonthPower.Size = new System.Drawing.Size(872, 26);
            this.ctrlMonthPower.StyleController = this.layoutControl2;
            this.ctrlMonthPower.TabIndex = 10;
            // 
            // ctrlDayPower
            // 
            this.ctrlDayPower.Location = new System.Drawing.Point(1328, 18);
            this.ctrlDayPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlDayPower.Name = "ctrlDayPower";
            this.ctrlDayPower.Properties.ReadOnly = true;
            this.ctrlDayPower.Size = new System.Drawing.Size(872, 26);
            this.ctrlDayPower.StyleController = this.layoutControl2;
            this.ctrlDayPower.TabIndex = 9;
            // 
            // ctrlMainTabControl
            // 
            this.ctrlMainTabControl.Location = new System.Drawing.Point(18, 146);
            this.ctrlMainTabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlMainTabControl.Name = "ctrlMainTabControl";
            this.ctrlMainTabControl.SelectedTabPage = this.tabPageRealKPI;
            this.ctrlMainTabControl.Size = new System.Drawing.Size(2182, 898);
            this.ctrlMainTabControl.TabIndex = 8;
            this.ctrlMainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageRealKPI});
            // 
            // tabPageRealKPI
            // 
            this.tabPageRealKPI.Controls.Add(this.layoutControl3);
            this.tabPageRealKPI.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageRealKPI.Name = "tabPageRealKPI";
            this.tabPageRealKPI.Size = new System.Drawing.Size(2180, 856);
            this.tabPageRealKPI.Text = "Στατιστικά Παραγωγής";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.grcDMYKPIData);
            this.layoutControl3.Controls.Add(this.grcHourlyData);
            this.layoutControl3.Controls.Add(this.ctrlTimeChooser);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(2180, 856);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // grcDMYKPIData
            // 
            this.grcDMYKPIData.DataSource = this.bsDMYData;
            this.grcDMYKPIData.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcDMYKPIData.Location = new System.Drawing.Point(18, 462);
            this.grcDMYKPIData.MainView = this.grvDMYKPIData;
            this.grcDMYKPIData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcDMYKPIData.Name = "grcDMYKPIData";
            this.grcDMYKPIData.Size = new System.Drawing.Size(2144, 376);
            this.grcDMYKPIData.TabIndex = 6;
            this.grcDMYKPIData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDMYKPIData});
            // 
            // bsDMYData
            // 
            this.bsDMYData.DataSource = typeof(HuaweiAPI.Utils.dKpiDataItem);
            // 
            // grvDMYKPIData
            // 
            this.grvDMYKPIData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDMYIndex,
            this.colDMYCollectDateTime,
            this.col_use_power,
            this.col_radiation_intensity1,
            this.col_reduction_total_co2,
            this.col_reduction_total_coal,
            this.col_theory_power1,
            this.col_ongrid_power1,
            this.col_power_profit1,
            this.col_inverter_power1,
            this.col_installed_capacity,
            this.col_perpower_ratio,
            this.col_reduction_total_tree,
            this.col_performance_ratio});
            this.grvDMYKPIData.DetailHeight = 538;
            this.grvDMYKPIData.FixedLineWidth = 3;
            this.grvDMYKPIData.GridControl = this.grcDMYKPIData;
            this.grvDMYKPIData.Name = "grvDMYKPIData";
            this.grvDMYKPIData.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.grvDMYKPIData.OptionsView.ShowFooter = true;
            this.grvDMYKPIData.OptionsView.ShowGroupPanel = false;
            this.grvDMYKPIData.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvDMYKPIData_CustomDrawCell);
            // 
            // colDMYIndex
            // 
            this.colDMYIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colDMYIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDMYIndex.Caption = "AA";
            this.colDMYIndex.MinWidth = 30;
            this.colDMYIndex.Name = "colDMYIndex";
            this.colDMYIndex.Visible = true;
            this.colDMYIndex.VisibleIndex = 0;
            this.colDMYIndex.Width = 114;
            // 
            // colDMYCollectDateTime
            // 
            this.colDMYCollectDateTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colDMYCollectDateTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDMYCollectDateTime.Caption = "Ημερομηνία";
            this.colDMYCollectDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDMYCollectDateTime.FieldName = "collectTime";
            this.colDMYCollectDateTime.MinWidth = 30;
            this.colDMYCollectDateTime.Name = "colDMYCollectDateTime";
            this.colDMYCollectDateTime.Visible = true;
            this.colDMYCollectDateTime.VisibleIndex = 1;
            this.colDMYCollectDateTime.Width = 151;
            // 
            // col_use_power
            // 
            this.col_use_power.AppearanceHeader.Options.UseTextOptions = true;
            this.col_use_power.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_use_power.Caption = "Κατανάλωση Ενέργειας (KWh)";
            this.col_use_power.DisplayFormat.FormatString = "n2";
            this.col_use_power.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_use_power.FieldName = "_use_power";
            this.col_use_power.MinWidth = 30;
            this.col_use_power.Name = "col_use_power";
            this.col_use_power.Visible = true;
            this.col_use_power.VisibleIndex = 2;
            this.col_use_power.Width = 151;
            // 
            // col_radiation_intensity1
            // 
            this.col_radiation_intensity1.AppearanceHeader.Options.UseTextOptions = true;
            this.col_radiation_intensity1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_radiation_intensity1.Caption = "Συνολική Ακτινοβολία (KWh/m2)";
            this.col_radiation_intensity1.DisplayFormat.FormatString = "n2";
            this.col_radiation_intensity1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_radiation_intensity1.FieldName = "_radiation_intensity";
            this.col_radiation_intensity1.MinWidth = 30;
            this.col_radiation_intensity1.Name = "col_radiation_intensity1";
            this.col_radiation_intensity1.Visible = true;
            this.col_radiation_intensity1.VisibleIndex = 3;
            this.col_radiation_intensity1.Width = 151;
            // 
            // col_reduction_total_co2
            // 
            this.col_reduction_total_co2.AppearanceHeader.Options.UseTextOptions = true;
            this.col_reduction_total_co2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_reduction_total_co2.Caption = "Συνολική Μείωση Ρύπων CO2";
            this.col_reduction_total_co2.DisplayFormat.FormatString = "n2";
            this.col_reduction_total_co2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_reduction_total_co2.FieldName = "_reduction_total_co2";
            this.col_reduction_total_co2.MinWidth = 30;
            this.col_reduction_total_co2.Name = "col_reduction_total_co2";
            this.col_reduction_total_co2.Visible = true;
            this.col_reduction_total_co2.VisibleIndex = 4;
            this.col_reduction_total_co2.Width = 151;
            // 
            // col_reduction_total_coal
            // 
            this.col_reduction_total_coal.AppearanceHeader.Options.UseTextOptions = true;
            this.col_reduction_total_coal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_reduction_total_coal.Caption = "Συνολική Μείωση Άνθρακα";
            this.col_reduction_total_coal.DisplayFormat.FormatString = "n2";
            this.col_reduction_total_coal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_reduction_total_coal.FieldName = "_reduction_total_coal";
            this.col_reduction_total_coal.MinWidth = 30;
            this.col_reduction_total_coal.Name = "col_reduction_total_coal";
            this.col_reduction_total_coal.Visible = true;
            this.col_reduction_total_coal.VisibleIndex = 5;
            this.col_reduction_total_coal.Width = 151;
            // 
            // col_theory_power1
            // 
            this.col_theory_power1.AppearanceHeader.Options.UseTextOptions = true;
            this.col_theory_power1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_theory_power1.Caption = "Θεωρητική Απόδοση (KWh)";
            this.col_theory_power1.DisplayFormat.FormatString = "n2";
            this.col_theory_power1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_theory_power1.FieldName = "_theory_power";
            this.col_theory_power1.MinWidth = 30;
            this.col_theory_power1.Name = "col_theory_power1";
            this.col_theory_power1.Visible = true;
            this.col_theory_power1.VisibleIndex = 6;
            this.col_theory_power1.Width = 151;
            // 
            // col_ongrid_power1
            // 
            this.col_ongrid_power1.AppearanceHeader.Options.UseTextOptions = true;
            this.col_ongrid_power1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_ongrid_power1.Caption = "Feed-in energy (KWh)";
            this.col_ongrid_power1.DisplayFormat.FormatString = "n2";
            this.col_ongrid_power1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_ongrid_power1.FieldName = "_ongrid_power";
            this.col_ongrid_power1.MinWidth = 30;
            this.col_ongrid_power1.Name = "col_ongrid_power1";
            this.col_ongrid_power1.Visible = true;
            this.col_ongrid_power1.VisibleIndex = 7;
            this.col_ongrid_power1.Width = 151;
            // 
            // col_power_profit1
            // 
            this.col_power_profit1.AppearanceHeader.Options.UseTextOptions = true;
            this.col_power_profit1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_power_profit1.Caption = "Έσοδα Ενέργειας (€)";
            this.col_power_profit1.DisplayFormat.FormatString = "n2";
            this.col_power_profit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_power_profit1.FieldName = "_power_profit";
            this.col_power_profit1.MinWidth = 30;
            this.col_power_profit1.Name = "col_power_profit1";
            this.col_power_profit1.Visible = true;
            this.col_power_profit1.VisibleIndex = 8;
            this.col_power_profit1.Width = 151;
            // 
            // col_inverter_power1
            // 
            this.col_inverter_power1.AppearanceHeader.Options.UseTextOptions = true;
            this.col_inverter_power1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_inverter_power1.Caption = "Ενέργεια Inverter (KWh)";
            this.col_inverter_power1.DisplayFormat.FormatString = "n2";
            this.col_inverter_power1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_inverter_power1.FieldName = "_inverter_power";
            this.col_inverter_power1.MinWidth = 30;
            this.col_inverter_power1.Name = "col_inverter_power1";
            this.col_inverter_power1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "_inverter_power", "{0:n0}")});
            this.col_inverter_power1.Visible = true;
            this.col_inverter_power1.VisibleIndex = 9;
            this.col_inverter_power1.Width = 151;
            // 
            // col_installed_capacity
            // 
            this.col_installed_capacity.AppearanceHeader.Options.UseTextOptions = true;
            this.col_installed_capacity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_installed_capacity.Caption = "Εγκατεστημένη Ισχύς (KW)";
            this.col_installed_capacity.DisplayFormat.FormatString = "n2";
            this.col_installed_capacity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_installed_capacity.FieldName = "_installed_capacity";
            this.col_installed_capacity.MinWidth = 30;
            this.col_installed_capacity.Name = "col_installed_capacity";
            this.col_installed_capacity.Visible = true;
            this.col_installed_capacity.VisibleIndex = 10;
            this.col_installed_capacity.Width = 151;
            // 
            // col_perpower_ratio
            // 
            this.col_perpower_ratio.AppearanceHeader.Options.UseTextOptions = true;
            this.col_perpower_ratio.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_perpower_ratio.Caption = "Ισοδύναμο Ωρών Λειτουργίας (Ώρες )";
            this.col_perpower_ratio.DisplayFormat.FormatString = "n2";
            this.col_perpower_ratio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_perpower_ratio.FieldName = "_perpower_ratio";
            this.col_perpower_ratio.MinWidth = 30;
            this.col_perpower_ratio.Name = "col_perpower_ratio";
            this.col_perpower_ratio.Visible = true;
            this.col_perpower_ratio.VisibleIndex = 11;
            this.col_perpower_ratio.Width = 151;
            // 
            // col_reduction_total_tree
            // 
            this.col_reduction_total_tree.AppearanceHeader.Options.UseTextOptions = true;
            this.col_reduction_total_tree.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_reduction_total_tree.Caption = "Συνολική Μείωση Δέντρων (Δέντρα)";
            this.col_reduction_total_tree.DisplayFormat.FormatString = "n2";
            this.col_reduction_total_tree.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_reduction_total_tree.FieldName = "_reduction_total_tree";
            this.col_reduction_total_tree.MinWidth = 30;
            this.col_reduction_total_tree.Name = "col_reduction_total_tree";
            this.col_reduction_total_tree.Visible = true;
            this.col_reduction_total_tree.VisibleIndex = 12;
            this.col_reduction_total_tree.Width = 151;
            // 
            // col_performance_ratio
            // 
            this.col_performance_ratio.AppearanceHeader.Options.UseTextOptions = true;
            this.col_performance_ratio.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_performance_ratio.Caption = "Αποδοτικότητα Παραγωγής Ηλεκτρικής Ενέργειας (KWh)";
            this.col_performance_ratio.DisplayFormat.FormatString = "n2";
            this.col_performance_ratio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_performance_ratio.FieldName = "_performance_ratio";
            this.col_performance_ratio.MinWidth = 30;
            this.col_performance_ratio.Name = "col_performance_ratio";
            this.col_performance_ratio.Visible = true;
            this.col_performance_ratio.VisibleIndex = 13;
            this.col_performance_ratio.Width = 171;
            // 
            // grcHourlyData
            // 
            this.grcHourlyData.DataSource = this.bsHourlyData;
            this.grcHourlyData.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcHourlyData.Location = new System.Drawing.Point(18, 50);
            this.grcHourlyData.MainView = this.grvHourlyData;
            this.grcHourlyData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grcHourlyData.Name = "grcHourlyData";
            this.grcHourlyData.Size = new System.Drawing.Size(2144, 406);
            this.grcHourlyData.TabIndex = 5;
            this.grcHourlyData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvHourlyData});
            // 
            // bsHourlyData
            // 
            this.bsHourlyData.DataSource = typeof(HuaweiAPI.Utils.hKpiDataItem);
            // 
            // grvHourlyData
            // 
            this.grvHourlyData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIndex,
            this.col_radiation_intensity,
            this.col_theory_power,
            this.col_inverter_power,
            this.col_ongrid_power,
            this.colCollectTime,
            this.col_power_profit});
            this.grvHourlyData.DetailHeight = 538;
            this.grvHourlyData.FixedLineWidth = 3;
            this.grvHourlyData.GridControl = this.grcHourlyData;
            this.grvHourlyData.Name = "grvHourlyData";
            this.grvHourlyData.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.grvHourlyData.OptionsView.ShowFooter = true;
            this.grvHourlyData.OptionsView.ShowGroupPanel = false;
            this.grvHourlyData.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvHourlyData_CustomDrawCell);
            // 
            // colIndex
            // 
            this.colIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.Caption = "ΑΑ";
            this.colIndex.MinWidth = 30;
            this.colIndex.Name = "colIndex";
            this.colIndex.Visible = true;
            this.colIndex.VisibleIndex = 0;
            this.colIndex.Width = 115;
            // 
            // col_radiation_intensity
            // 
            this.col_radiation_intensity.AppearanceHeader.Options.UseTextOptions = true;
            this.col_radiation_intensity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_radiation_intensity.Caption = "Συνολική Ακτινοβολία (KWh/m2)";
            this.col_radiation_intensity.DisplayFormat.FormatString = "n2";
            this.col_radiation_intensity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_radiation_intensity.FieldName = "_radiation_intensity";
            this.col_radiation_intensity.MinWidth = 30;
            this.col_radiation_intensity.Name = "col_radiation_intensity";
            this.col_radiation_intensity.Visible = true;
            this.col_radiation_intensity.VisibleIndex = 1;
            this.col_radiation_intensity.Width = 373;
            // 
            // col_theory_power
            // 
            this.col_theory_power.AppearanceHeader.Options.UseTextOptions = true;
            this.col_theory_power.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_theory_power.Caption = "Θεωρητική Απόδοση (KWh)";
            this.col_theory_power.DisplayFormat.FormatString = "n2";
            this.col_theory_power.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_theory_power.FieldName = "_theory_power";
            this.col_theory_power.MinWidth = 30;
            this.col_theory_power.Name = "col_theory_power";
            this.col_theory_power.Visible = true;
            this.col_theory_power.VisibleIndex = 2;
            this.col_theory_power.Width = 373;
            // 
            // col_inverter_power
            // 
            this.col_inverter_power.AppearanceHeader.Options.UseTextOptions = true;
            this.col_inverter_power.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_inverter_power.Caption = "Ενέργεια Inverter (KWh)";
            this.col_inverter_power.DisplayFormat.FormatString = "n2";
            this.col_inverter_power.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_inverter_power.FieldName = "_inverter_power";
            this.col_inverter_power.MinWidth = 30;
            this.col_inverter_power.Name = "col_inverter_power";
            this.col_inverter_power.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "_inverter_power", "{0:n0}")});
            this.col_inverter_power.Visible = true;
            this.col_inverter_power.VisibleIndex = 3;
            this.col_inverter_power.Width = 373;
            // 
            // col_ongrid_power
            // 
            this.col_ongrid_power.AppearanceHeader.Options.UseTextOptions = true;
            this.col_ongrid_power.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_ongrid_power.Caption = "Feed-in energy (KWh)";
            this.col_ongrid_power.DisplayFormat.FormatString = "n2";
            this.col_ongrid_power.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_ongrid_power.FieldName = "_ongrid_power";
            this.col_ongrid_power.MinWidth = 30;
            this.col_ongrid_power.Name = "col_ongrid_power";
            this.col_ongrid_power.Visible = true;
            this.col_ongrid_power.VisibleIndex = 4;
            this.col_ongrid_power.Width = 373;
            // 
            // colCollectTime
            // 
            this.colCollectTime.Caption = "Ημερομηνία";
            this.colCollectTime.DisplayFormat.FormatString = "g";
            this.colCollectTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCollectTime.FieldName = "collectTime";
            this.colCollectTime.MinWidth = 30;
            this.colCollectTime.Name = "colCollectTime";
            this.colCollectTime.Visible = true;
            this.colCollectTime.VisibleIndex = 5;
            this.colCollectTime.Width = 249;
            // 
            // col_power_profit
            // 
            this.col_power_profit.AppearanceHeader.Options.UseTextOptions = true;
            this.col_power_profit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_power_profit.Caption = "Έσοδα Ενέργειας (€)";
            this.col_power_profit.DisplayFormat.FormatString = "n2";
            this.col_power_profit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_power_profit.FieldName = "_power_profit";
            this.col_power_profit.MinWidth = 30;
            this.col_power_profit.Name = "col_power_profit";
            this.col_power_profit.Visible = true;
            this.col_power_profit.VisibleIndex = 6;
            this.col_power_profit.Width = 244;
            // 
            // ctrlTimeChooser
            // 
            this.ctrlTimeChooser.Location = new System.Drawing.Point(49, 18);
            this.ctrlTimeChooser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlTimeChooser.Name = "ctrlTimeChooser";
            this.ctrlTimeChooser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ctrlTimeChooser.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("desc", "Περιγραφή")});
            this.ctrlTimeChooser.Properties.DataSource = this.bsTimes;
            this.ctrlTimeChooser.Properties.DisplayMember = "desc";
            this.ctrlTimeChooser.Properties.NullText = "";
            this.ctrlTimeChooser.Properties.ValueMember = "value";
            this.ctrlTimeChooser.Size = new System.Drawing.Size(220, 26);
            this.ctrlTimeChooser.StyleController = this.layoutControl3;
            this.ctrlTimeChooser.TabIndex = 4;
            this.ctrlTimeChooser.EditValueChanged += new System.EventHandler(this.ctrlTimeChooser_EditValueChanged);
            // 
            // bsTimes
            // 
            this.bsTimes.DataSource = typeof(HuaweiAPI.Forms.stationKPIs.timeObj);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.loTimeChooser,
            this.loHourlyData,
            this.emptySpaceItem1,
            this.loDMYData});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(2180, 856);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // loTimeChooser
            // 
            this.loTimeChooser.Control = this.ctrlTimeChooser;
            this.loTimeChooser.Location = new System.Drawing.Point(0, 0);
            this.loTimeChooser.Name = "loTimeChooser";
            this.loTimeChooser.Size = new System.Drawing.Size(257, 32);
            this.loTimeChooser.Text = "Ανά";
            this.loTimeChooser.TextSize = new System.Drawing.Size(27, 20);
            // 
            // loHourlyData
            // 
            this.loHourlyData.Control = this.grcHourlyData;
            this.loHourlyData.Location = new System.Drawing.Point(0, 32);
            this.loHourlyData.Name = "loHourlyData";
            this.loHourlyData.Size = new System.Drawing.Size(2150, 412);
            this.loHourlyData.TextSize = new System.Drawing.Size(0, 0);
            this.loHourlyData.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(257, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1893, 32);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // loDMYData
            // 
            this.loDMYData.Control = this.grcDMYKPIData;
            this.loDMYData.Location = new System.Drawing.Point(0, 444);
            this.loDMYData.Name = "loDMYData";
            this.loDMYData.Size = new System.Drawing.Size(2150, 382);
            this.loDMYData.TextSize = new System.Drawing.Size(0, 0);
            this.loDMYData.TextVisible = false;
            // 
            // ctrlProjectAddress
            // 
            this.ctrlProjectAddress.Location = new System.Drawing.Point(191, 114);
            this.ctrlProjectAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlProjectAddress.Name = "ctrlProjectAddress";
            this.ctrlProjectAddress.Properties.ReadOnly = true;
            this.ctrlProjectAddress.Size = new System.Drawing.Size(853, 26);
            this.ctrlProjectAddress.StyleController = this.layoutControl2;
            this.ctrlProjectAddress.TabIndex = 7;
            // 
            // ctrlProjectDesc
            // 
            this.ctrlProjectDesc.Location = new System.Drawing.Point(191, 50);
            this.ctrlProjectDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlProjectDesc.Name = "ctrlProjectDesc";
            this.ctrlProjectDesc.Properties.ReadOnly = true;
            this.ctrlProjectDesc.Size = new System.Drawing.Size(853, 26);
            this.ctrlProjectDesc.StyleController = this.layoutControl2;
            this.ctrlProjectDesc.TabIndex = 6;
            // 
            // ctrlProjectCapacity
            // 
            this.ctrlProjectCapacity.Location = new System.Drawing.Point(191, 82);
            this.ctrlProjectCapacity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlProjectCapacity.Name = "ctrlProjectCapacity";
            this.ctrlProjectCapacity.Properties.ReadOnly = true;
            this.ctrlProjectCapacity.Size = new System.Drawing.Size(853, 26);
            this.ctrlProjectCapacity.StyleController = this.layoutControl2;
            this.ctrlProjectCapacity.TabIndex = 5;
            // 
            // ctrlProjectCode
            // 
            this.ctrlProjectCode.Location = new System.Drawing.Point(191, 18);
            this.ctrlProjectCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ctrlProjectCode.Name = "ctrlProjectCode";
            this.ctrlProjectCode.Properties.ReadOnly = true;
            this.ctrlProjectCode.Size = new System.Drawing.Size(853, 26);
            this.ctrlProjectCode.StyleController = this.layoutControl2;
            this.ctrlProjectCode.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.loProjectCode,
            this.loProjectDesc,
            this.loProjectCapacity,
            this.loProjectAddress,
            this.emptySpaceItem2,
            this.loMainTabControl,
            this.loDayPower,
            this.loMonthPower,
            this.loTotalPower,
            this.loHealthyStatus});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(2218, 1062);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // loProjectCode
            // 
            this.loProjectCode.Control = this.ctrlProjectCode;
            this.loProjectCode.Location = new System.Drawing.Point(0, 0);
            this.loProjectCode.Name = "loProjectCode";
            this.loProjectCode.Size = new System.Drawing.Size(1032, 32);
            this.loProjectCode.Text = "Κωδικός Έργου";
            this.loProjectCode.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loProjectDesc
            // 
            this.loProjectDesc.Control = this.ctrlProjectDesc;
            this.loProjectDesc.Location = new System.Drawing.Point(0, 32);
            this.loProjectDesc.Name = "loProjectDesc";
            this.loProjectDesc.Size = new System.Drawing.Size(1032, 32);
            this.loProjectDesc.Text = "Περιγραφή Έργου ";
            this.loProjectDesc.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loProjectCapacity
            // 
            this.loProjectCapacity.Control = this.ctrlProjectCapacity;
            this.loProjectCapacity.Location = new System.Drawing.Point(0, 64);
            this.loProjectCapacity.Name = "loProjectCapacity";
            this.loProjectCapacity.Size = new System.Drawing.Size(1032, 32);
            this.loProjectCapacity.Text = "Εγκατεστημένη Ισχύς";
            this.loProjectCapacity.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loProjectAddress
            // 
            this.loProjectAddress.Control = this.ctrlProjectAddress;
            this.loProjectAddress.Location = new System.Drawing.Point(0, 96);
            this.loProjectAddress.Name = "loProjectAddress";
            this.loProjectAddress.Size = new System.Drawing.Size(1032, 32);
            this.loProjectAddress.Text = "Περιοχή Εγκατάστασης";
            this.loProjectAddress.TextSize = new System.Drawing.Size(169, 20);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(1032, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(105, 128);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // loMainTabControl
            // 
            this.loMainTabControl.Control = this.ctrlMainTabControl;
            this.loMainTabControl.Location = new System.Drawing.Point(0, 128);
            this.loMainTabControl.Name = "loMainTabControl";
            this.loMainTabControl.Size = new System.Drawing.Size(2188, 904);
            this.loMainTabControl.TextSize = new System.Drawing.Size(0, 0);
            this.loMainTabControl.TextVisible = false;
            // 
            // loDayPower
            // 
            this.loDayPower.Control = this.ctrlDayPower;
            this.loDayPower.Location = new System.Drawing.Point(1137, 0);
            this.loDayPower.Name = "loDayPower";
            this.loDayPower.Size = new System.Drawing.Size(1051, 32);
            this.loDayPower.Text = "Ημερήσια Παραγωγή";
            this.loDayPower.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loMonthPower
            // 
            this.loMonthPower.Control = this.ctrlMonthPower;
            this.loMonthPower.Location = new System.Drawing.Point(1137, 32);
            this.loMonthPower.Name = "loMonthPower";
            this.loMonthPower.Size = new System.Drawing.Size(1051, 32);
            this.loMonthPower.Text = "Μηνιαία Παραγωγή";
            this.loMonthPower.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loTotalPower
            // 
            this.loTotalPower.Control = this.ctrlTotalPower;
            this.loTotalPower.Location = new System.Drawing.Point(1137, 64);
            this.loTotalPower.Name = "loTotalPower";
            this.loTotalPower.Size = new System.Drawing.Size(1051, 32);
            this.loTotalPower.Text = "Συνολική Παραγωγή";
            this.loTotalPower.TextSize = new System.Drawing.Size(169, 20);
            // 
            // loHealthyStatus
            // 
            this.loHealthyStatus.Control = this.ctrlHealthyStatus;
            this.loHealthyStatus.Location = new System.Drawing.Point(1137, 96);
            this.loHealthyStatus.Name = "loHealthyStatus";
            this.loHealthyStatus.Size = new System.Drawing.Size(1051, 32);
            this.loHealthyStatus.Text = "Κατάσταση Έργου";
            this.loHealthyStatus.TextSize = new System.Drawing.Size(169, 20);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(2254, 1098);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.layoutControl2;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(2224, 1068);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // stationKPIs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2254, 1098);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "stationKPIs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "stationKPIs";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctrlHealthyStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTotalPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMonthPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlDayPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlMainTabControl)).EndInit();
            this.ctrlMainTabControl.ResumeLayout(false);
            this.tabPageRealKPI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDMYKPIData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDMYData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDMYKPIData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcHourlyData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsHourlyData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvHourlyData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlTimeChooser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTimes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTimeChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loHourlyData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loDMYData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loProjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMainTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loDayPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loMonthPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loTotalPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loHealthyStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit ctrlProjectAddress;
        private DevExpress.XtraEditors.TextEdit ctrlProjectDesc;
        private DevExpress.XtraEditors.TextEdit ctrlProjectCapacity;
        private DevExpress.XtraEditors.TextEdit ctrlProjectCode;
        private DevExpress.XtraLayout.LayoutControlItem loProjectCode;
        private DevExpress.XtraLayout.LayoutControlItem loProjectDesc;
        private DevExpress.XtraLayout.LayoutControlItem loProjectCapacity;
        private DevExpress.XtraLayout.LayoutControlItem loProjectAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraTab.XtraTabControl ctrlMainTabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageRealKPI;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem loMainTabControl;
        private DevExpress.XtraEditors.TextEdit ctrlTotalPower;
        private DevExpress.XtraEditors.TextEdit ctrlMonthPower;
        private DevExpress.XtraEditors.TextEdit ctrlDayPower;
        private DevExpress.XtraLayout.LayoutControlItem loDayPower;
        private DevExpress.XtraLayout.LayoutControlItem loMonthPower;
        private DevExpress.XtraLayout.LayoutControlItem loTotalPower;
        private DevExpress.XtraEditors.TextEdit ctrlHealthyStatus;
        private DevExpress.XtraLayout.LayoutControlItem loHealthyStatus;
        private DevExpress.XtraGrid.GridControl grcHourlyData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvHourlyData;
        private DevExpress.XtraEditors.LookUpEdit ctrlTimeChooser;
        private DevExpress.XtraLayout.LayoutControlItem loTimeChooser;
        private DevExpress.XtraLayout.LayoutControlItem loHourlyData;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource bsHourlyData;
        private System.Windows.Forms.BindingSource bsTimes;
        private DevExpress.XtraGrid.Columns.GridColumn colIndex;
        private DevExpress.XtraGrid.Columns.GridColumn col_radiation_intensity;
        private DevExpress.XtraGrid.Columns.GridColumn col_theory_power;
        private DevExpress.XtraGrid.Columns.GridColumn col_inverter_power;
        private DevExpress.XtraGrid.Columns.GridColumn col_ongrid_power;
        private DevExpress.XtraGrid.Columns.GridColumn col_power_profit;
        private DevExpress.XtraGrid.Columns.GridColumn colCollectTime;
        private DevExpress.XtraGrid.GridControl grcDMYKPIData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDMYKPIData;
        private DevExpress.XtraLayout.LayoutControlItem loDMYData;
        private System.Windows.Forms.BindingSource bsDMYData;
        private DevExpress.XtraGrid.Columns.GridColumn colDMYIndex;
        private DevExpress.XtraGrid.Columns.GridColumn col_use_power;
        private DevExpress.XtraGrid.Columns.GridColumn col_radiation_intensity1;
        private DevExpress.XtraGrid.Columns.GridColumn col_reduction_total_co2;
        private DevExpress.XtraGrid.Columns.GridColumn col_reduction_total_coal;
        private DevExpress.XtraGrid.Columns.GridColumn col_theory_power1;
        private DevExpress.XtraGrid.Columns.GridColumn col_ongrid_power1;
        private DevExpress.XtraGrid.Columns.GridColumn col_power_profit1;
        private DevExpress.XtraGrid.Columns.GridColumn col_installed_capacity;
        private DevExpress.XtraGrid.Columns.GridColumn col_perpower_ratio;
        private DevExpress.XtraGrid.Columns.GridColumn col_inverter_power1;
        private DevExpress.XtraGrid.Columns.GridColumn col_reduction_total_tree;
        private DevExpress.XtraGrid.Columns.GridColumn col_performance_ratio;
        private DevExpress.XtraGrid.Columns.GridColumn colDMYCollectDateTime;
    }
}