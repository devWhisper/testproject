﻿
namespace HuaweiAPI.Forms
{
    partial class stationDevices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.grcDetail = new DevExpress.XtraGrid.GridControl();
            this.bsDetail = new System.Windows.Forms.BindingSource(this.components);
            this.grvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colalarmName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrepairSuggestion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcauseId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colalarmCause = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colalarmType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colraiseDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_raiseTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colraiseTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colalarmId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grcMain = new DevExpress.XtraGrid.GridControl();
            this.bsMain = new System.Windows.Forms.BindingSource(this.components);
            this.grvMain = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldevName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colesnCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldevTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsoftwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colinvType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.grcDetail);
            this.layoutControl1.Controls.Add(this.grcMain);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1290, 649);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // grcDetail
            // 
            this.grcDetail.DataSource = this.bsDetail;
            this.grcDetail.Location = new System.Drawing.Point(12, 355);
            this.grcDetail.MainView = this.grvDetail;
            this.grcDetail.Name = "grcDetail";
            this.grcDetail.Size = new System.Drawing.Size(1266, 282);
            this.grcDetail.TabIndex = 5;
            this.grcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDetail});
            // 
            // bsDetail
            // 
            this.bsDetail.DataSource = typeof(HuaweiAPI.Utils.deviceAlarm);
            // 
            // grvDetail
            // 
            this.grvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colalarmName,
            this.colrepairSuggestion,
            this.colcauseId,
            this.colalarmCause,
            this.colalarmType,
            this.colraiseDateTime,
            this.col_raiseTime,
            this.colraiseTime,
            this.colalarmId,
            this.colstationName,
            this.collev,
            this.colstatus});
            this.grvDetail.GridControl = this.grcDetail;
            this.grvDetail.Name = "grvDetail";
            // 
            // colalarmName
            // 
            this.colalarmName.FieldName = "alarmName";
            this.colalarmName.Name = "colalarmName";
            this.colalarmName.Visible = true;
            this.colalarmName.VisibleIndex = 0;
            // 
            // colrepairSuggestion
            // 
            this.colrepairSuggestion.FieldName = "repairSuggestion";
            this.colrepairSuggestion.Name = "colrepairSuggestion";
            this.colrepairSuggestion.Visible = true;
            this.colrepairSuggestion.VisibleIndex = 1;
            // 
            // colcauseId
            // 
            this.colcauseId.FieldName = "causeId";
            this.colcauseId.Name = "colcauseId";
            this.colcauseId.Visible = true;
            this.colcauseId.VisibleIndex = 2;
            // 
            // colalarmCause
            // 
            this.colalarmCause.FieldName = "alarmCause";
            this.colalarmCause.Name = "colalarmCause";
            this.colalarmCause.Visible = true;
            this.colalarmCause.VisibleIndex = 3;
            // 
            // colalarmType
            // 
            this.colalarmType.FieldName = "alarmType";
            this.colalarmType.Name = "colalarmType";
            this.colalarmType.Visible = true;
            this.colalarmType.VisibleIndex = 4;
            // 
            // colraiseDateTime
            // 
            this.colraiseDateTime.FieldName = "raiseDateTime";
            this.colraiseDateTime.Name = "colraiseDateTime";
            this.colraiseDateTime.Visible = true;
            this.colraiseDateTime.VisibleIndex = 5;
            // 
            // col_raiseTime
            // 
            this.col_raiseTime.FieldName = "_raiseTime";
            this.col_raiseTime.Name = "col_raiseTime";
            this.col_raiseTime.Visible = true;
            this.col_raiseTime.VisibleIndex = 6;
            // 
            // colraiseTime
            // 
            this.colraiseTime.FieldName = "raiseTime";
            this.colraiseTime.Name = "colraiseTime";
            this.colraiseTime.Visible = true;
            this.colraiseTime.VisibleIndex = 7;
            // 
            // colalarmId
            // 
            this.colalarmId.FieldName = "alarmId";
            this.colalarmId.Name = "colalarmId";
            this.colalarmId.Visible = true;
            this.colalarmId.VisibleIndex = 8;
            // 
            // colstationName
            // 
            this.colstationName.FieldName = "stationName";
            this.colstationName.Name = "colstationName";
            this.colstationName.Visible = true;
            this.colstationName.VisibleIndex = 9;
            // 
            // collev
            // 
            this.collev.FieldName = "lev";
            this.collev.Name = "collev";
            this.collev.Visible = true;
            this.collev.VisibleIndex = 10;
            // 
            // colstatus
            // 
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.Visible = true;
            this.colstatus.VisibleIndex = 11;
            // 
            // grcMain
            // 
            this.grcMain.DataSource = this.bsMain;
            this.grcMain.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.grcMain.Location = new System.Drawing.Point(12, 12);
            this.grcMain.MainView = this.grvMain;
            this.grcMain.Margin = new System.Windows.Forms.Padding(2);
            this.grcMain.Name = "grcMain";
            this.grcMain.Size = new System.Drawing.Size(1266, 339);
            this.grcMain.TabIndex = 4;
            this.grcMain.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvMain});
            // 
            // bsMain
            // 
            this.bsMain.DataSource = typeof(HuaweiAPI.Utils.device);
            // 
            // grvMain
            // 
            this.grvMain.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIndex,
            this.coldevName,
            this.colesnCode,
            this.coldevTypeId,
            this.colsoftwareVersion,
            this.colinvType});
            this.grvMain.DetailHeight = 227;
            this.grvMain.FixedLineWidth = 1;
            this.grvMain.GridControl = this.grcMain;
            this.grvMain.Name = "grvMain";
            this.grvMain.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.grvMain.OptionsView.ShowFooter = true;
            this.grvMain.OptionsView.ShowGroupPanel = false;
            this.grvMain.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.grvMain_CustomDrawCell);
            this.grvMain.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grvMain_FocusedRowChanged);
            // 
            // colIndex
            // 
            this.colIndex.AppearanceHeader.Options.UseTextOptions = true;
            this.colIndex.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIndex.Caption = "ΑΑ";
            this.colIndex.Name = "colIndex";
            this.colIndex.OptionsColumn.AllowEdit = false;
            this.colIndex.Visible = true;
            this.colIndex.VisibleIndex = 0;
            this.colIndex.Width = 93;
            // 
            // coldevName
            // 
            this.coldevName.AppearanceHeader.Options.UseTextOptions = true;
            this.coldevName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coldevName.Caption = "Όνομα Συσκευής";
            this.coldevName.FieldName = "devName";
            this.coldevName.Name = "coldevName";
            this.coldevName.OptionsColumn.AllowEdit = false;
            this.coldevName.Visible = true;
            this.coldevName.VisibleIndex = 1;
            this.coldevName.Width = 301;
            // 
            // colesnCode
            // 
            this.colesnCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colesnCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colesnCode.Caption = "SN";
            this.colesnCode.FieldName = "esnCode";
            this.colesnCode.Name = "colesnCode";
            this.colesnCode.OptionsColumn.AllowEdit = false;
            this.colesnCode.Visible = true;
            this.colesnCode.VisibleIndex = 2;
            this.colesnCode.Width = 301;
            // 
            // coldevTypeId
            // 
            this.coldevTypeId.AppearanceHeader.Options.UseTextOptions = true;
            this.coldevTypeId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coldevTypeId.Caption = "Τύπος Συσκευής";
            this.coldevTypeId.FieldName = "sdevTypeId";
            this.coldevTypeId.Name = "coldevTypeId";
            this.coldevTypeId.OptionsColumn.AllowEdit = false;
            this.coldevTypeId.Visible = true;
            this.coldevTypeId.VisibleIndex = 3;
            this.coldevTypeId.Width = 301;
            // 
            // colsoftwareVersion
            // 
            this.colsoftwareVersion.AppearanceHeader.Options.UseTextOptions = true;
            this.colsoftwareVersion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsoftwareVersion.Caption = "Έκδοση Λογισμικού";
            this.colsoftwareVersion.FieldName = "softwareVersion";
            this.colsoftwareVersion.Name = "colsoftwareVersion";
            this.colsoftwareVersion.OptionsColumn.AllowEdit = false;
            this.colsoftwareVersion.Visible = true;
            this.colsoftwareVersion.VisibleIndex = 4;
            this.colsoftwareVersion.Width = 301;
            // 
            // colinvType
            // 
            this.colinvType.AppearanceHeader.Options.UseTextOptions = true;
            this.colinvType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colinvType.Caption = "Τύπος Inverter";
            this.colinvType.FieldName = "invType";
            this.colinvType.Name = "colinvType";
            this.colinvType.OptionsColumn.AllowEdit = false;
            this.colinvType.Visible = true;
            this.colinvType.VisibleIndex = 5;
            this.colinvType.Width = 302;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1290, 649);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.grcMain;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1270, 343);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.grcDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 343);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1270, 286);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // stationDevices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 649);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "stationDevices";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Εξοπλισμός Πάρκου";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraGrid.GridControl grcMain;
        private DevExpress.XtraGrid.Views.Grid.GridView grvMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource bsMain;
        private DevExpress.XtraGrid.Columns.GridColumn colIndex;
        private DevExpress.XtraGrid.Columns.GridColumn coldevName;
        private DevExpress.XtraGrid.Columns.GridColumn colesnCode;
        private DevExpress.XtraGrid.Columns.GridColumn coldevTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colsoftwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colinvType;
        private DevExpress.XtraGrid.GridControl grcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDetail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource bsDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colalarmName;
        private DevExpress.XtraGrid.Columns.GridColumn colrepairSuggestion;
        private DevExpress.XtraGrid.Columns.GridColumn colcauseId;
        private DevExpress.XtraGrid.Columns.GridColumn colalarmCause;
        private DevExpress.XtraGrid.Columns.GridColumn colalarmType;
        private DevExpress.XtraGrid.Columns.GridColumn colraiseDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn col_raiseTime;
        private DevExpress.XtraGrid.Columns.GridColumn colraiseTime;
        private DevExpress.XtraGrid.Columns.GridColumn colalarmId;
        private DevExpress.XtraGrid.Columns.GridColumn colstationName;
        private DevExpress.XtraGrid.Columns.GridColumn collev;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
    }
}