﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HuaweiAPI.Utils;
using SLnet.Base.DbUtils;
using System.Security;
using HuaweiAPI.Forms;

namespace HuaweiAPI.Forms
{
    public partial class stationDevices : Base
    {
        public station currentItem = new station();
        public Connection connection = (Connection)null;

        public stationDevices()
        {
            InitializeComponent();
        }

        public void setDevs()
        {
            List<device> dummyLst = new List<device>();
            bsMain.DataSource = dummyLst;
            if (currentItem == null)
                return;
            dummyLst = currentItem.devFactoryLst(connection);
            bsMain.DataSource = dummyLst;
        }//setDevs

        private void grvMain_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.Name == colIndex.Name)
            {
                e.DisplayText = (e.RowHandle + 1).ToString() + ".";
                e.CellValue = (e.RowHandle + 1);
            }
        }//grvMain_CustomDrawCell

        private void grvMain_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            device d = grvMain.GetFocusedRow() as device;
            List<deviceAlarm> dummyLst = new List<deviceAlarm>();
            bsDetail.DataSource = dummyLst;
            if (d == null)
                return;
            dummyLst = d.deviceAlarmFactoryLst(connection);
            bsDetail.DataSource = dummyLst;
        }//grvMain_FocusedRowChanged
    }
}
