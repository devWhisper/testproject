﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security;
using org.apache.pdfbox.pdmodel;
using HuaweiAPI.Utils;
using SLnet.Base.DbUtils;
using HuaweiAPI.Forms;

namespace HuaweiAPI
{
    public partial class Main : Forms.Base
    {
        string url = "https://eu5.fusionsolar.huawei.com/thirdData/";
        auth a = new auth();
        static bool isDev = false;
        string connStr = isDev ? "Data Source=devsql;Initial Catalog=DEV_GLX_AGRO;uid=sa;pwd=Agr0!23$;" : "Data Source=agrosql2;Initial Catalog=GLX_AGRO;uid=sa;pwd=Agr0!23$;";
        Connection connection;
        interConnection intcn;

        public Main()
        {
            InitializeComponent();
            intcn = new interConnection(connStr);
            connection = intcn.connection;
            connection.connect();
            setLastUpdate();
            showStations();
        }//Main

        private void setLastUpdate()
        {
            DataTable dt = connection.ExecuteResultSet("select distinct top 1 ETA05COLLECTDATETIME from ETA05PVMONITORREALKPI order by ETA05COLLECTDATETIME desc", new slQueryParameters());
            if (dt.Rows.Count > 0)
            {
                DateTime lupdt = dt.Rows[0]["ETA05COLLECTDATETIME"] != null && !string.IsNullOrEmpty(dt.Rows[0]["ETA05COLLECTDATETIME"].ToString()) ? DateTime.Parse(dt.Rows[0]["ETA05COLLECTDATETIME"].ToString()) : DateTime.Parse("1/1/1970");
                ctrlLastUpdate.Text = lupdt.Hour.ToString("00") + ":" + lupdt.Minute.ToString("00") + ":" + lupdt.Second.ToString("00") + "  "  + lupdt.Day.ToString("00") + "/" + lupdt.Month.ToString("00") + "/" + lupdt.Year.ToString("0000");
            }
        }//setLastUpdate
        
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            connection.disconnect();
        }//Main_FormClosing

        private void ctrlUpdateBtn_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            updateData();
            Cursor = Cursors.Default;
        }//ctrlUpdateBtn_Click

        public void updateData()
        {
            ctrlUpdateBtn.Enabled = false;
            while (!intcn.checkInterval())
            {
                System.Threading.Thread.Sleep(2000);
            }
            a = new auth(url, "greenline_energy", "12345@Huawei");
            a.pushLogin();
            connection.writeEvent("New API connection");
            stationLst sList = new stationLst(a, connection);
            sList.postKPIs();
            deviceLst dList = new deviceLst(a, connection);
            dList.updateDevices();
            dList.postDevicesAlarms();
            setLastUpdate();
            showStations();
            ctrlUpdateBtn.Enabled = true;
        }//updateData

        private void showStations()
        {
            List<station> sts = new List<station>();
            string sql = @" select * from ETA05PVMONITORSTATIONS station
                            inner join GXPROJECT project on project.GXCODE = station.ETA05STATIONNAME
                            order by project.gxcode";
            slQueryParameters prms = new slQueryParameters();
            DataTable dt = connection.ExecuteResultSet(sql, prms);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    station s = new station();
                    s.stationID = dt.Rows[i]["etA05ID"] != null && !string.IsNullOrEmpty(dt.Rows[i]["etA05ID"].ToString()) ? Guid.Parse(dt.Rows[i]["etA05ID"].ToString()) : Guid.Empty;
                    s.stationCode = dt.Rows[i]["ETA05STATIONCODE"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05STATIONCODE"].ToString()) ? dt.Rows[i]["ETA05STATIONCODE"].ToString() : string.Empty;
                    s.stationName = dt.Rows[i]["ETA05STATIONNAME"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05STATIONNAME"].ToString()) ? dt.Rows[i]["ETA05STATIONNAME"].ToString() : string.Empty;
                    s.stationAddr = dt.Rows[i]["ETA05STATIONADDR"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05STATIONADDR"].ToString()) ? dt.Rows[i]["ETA05STATIONADDR"].ToString() : string.Empty;
                    s.capacity = dt.Rows[i]["ETA05CAPACITY"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05CAPACITY"].ToString()) ? double.Parse(dt.Rows[i]["ETA05CAPACITY"].ToString()) : 0;
                    s.buildState = dt.Rows[i]["eta05buildstate"] != null && !string.IsNullOrEmpty(dt.Rows[i]["eta05buildstate"].ToString()) ? int.Parse(dt.Rows[i]["eta05buildstate"].ToString()) : -1;
                    s.combineType = dt.Rows[i]["ETA05COMBINETYPE"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05COMBINETYPE"].ToString()) ? int.Parse(dt.Rows[i]["ETA05COMBINETYPE"].ToString()) : -1;
                    s.aidType = dt.Rows[i]["ETA05AIDTYPE"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05AIDTYPE"].ToString()) ? int.Parse(dt.Rows[i]["ETA05AIDTYPE"].ToString()) : -1;
                    s.stationLinkman = dt.Rows[i]["ETA05STATIONLINKMAN"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05STATIONLINKMAN"].ToString()) ? dt.Rows[i]["ETA05STATIONLINKMAN"].ToString() : string.Empty;
                    s.linkmanPho = dt.Rows[i]["ETA05LINKMANPHO"] != null && !string.IsNullOrEmpty(dt.Rows[i]["ETA05LINKMANPHO"].ToString()) ? dt.Rows[i]["ETA05LINKMANPHO"].ToString() : string.Empty;
                    s.projectName = dt.Rows[i]["GXDESCRIPTION"] != null && !string.IsNullOrEmpty(dt.Rows[i]["GXDESCRIPTION"].ToString()) ? dt.Rows[i]["GXDESCRIPTION"].ToString() : string.Empty;
                    s.realkpi = new kpi();
                    prms = new slQueryParameters();
                    prms.Add("@lastUpdate", DateTime.Parse(ctrlLastUpdate.Text), DbType.DateTime);
                    prms.Add("@stationID", s.stationID, DbType.Guid);
                    sql = @" select top 1 * from ETA05PVMONITORREALKPI rkpi
                             where 1=1
                             and rkpi.ETA05STATIONID = @stationID
                             order by ETA05COLLECTDATETIME desc
                                ";
                    DataTable ddt = connection.ExecuteResultSet(sql, prms); 
                    if (ddt.Rows.Count > 0)
                    {
                        s.realkpi.stationCode = s.stationCode;
                        s.realkpi.errorMessage = string.Empty;
                        s.realkpi.dataItemMap.day_power = ddt.Rows[0]["eta05Day_Power"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["eta05Day_Power"].ToString()) ? double.Parse(ddt.Rows[0]["eta05Day_Power"].ToString()) : 0;
                        s.realkpi.dataItemMap.month_power = ddt.Rows[0]["eta05month_power"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["eta05month_power"].ToString()) ? double.Parse(ddt.Rows[0]["eta05month_power"].ToString()) : 0;
                        s.realkpi.dataItemMap.total_power = ddt.Rows[0]["eta05total_power"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["eta05total_power"].ToString()) ? double.Parse(ddt.Rows[0]["eta05total_power"].ToString()) : 0;
                        s.realkpi.dataItemMap.real_health_state = ddt.Rows[0]["eta05real_health_state"] != null && !string.IsNullOrEmpty(ddt.Rows[0]["eta05real_health_state"].ToString()) ? int.Parse(ddt.Rows[0]["eta05real_health_state"].ToString()) : -1;
                    }
                    sts.Add(s);
                }
            }
            ctrlTotalDailyPower.Text = (sts.Sum(x => x.realkpi.dataItemMap.day_power) / 1000).ToString("n2") + " MWh";
            ctrlTotalCapacity.Text = (sts.Sum(x => x.capacity) / 1000).ToString("n2") + " MW";
            ctrlTotalMonthPower.Text = (sts.Sum(x => x.realkpi.dataItemMap.month_power) / 1000).ToString("n2") + " MWh";
            ctrlTotalPower.Text = (sts.Sum(x => x.realkpi.dataItemMap.total_power) / 1000000).ToString("n2") + " GWh";
            bsStations.DataSource = sts;
        }//showStations

        private void grvStations_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.Name == colIndex.Name)
            {
                e.DisplayText = (e.RowHandle + 1).ToString() + ".";
                e.CellValue = (e.RowHandle + 1);
            }
        }//grvStations_CustomDrawCell

        private void repStation_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            stationKPIs myForm = new stationKPIs();
            myForm.currentItem = grvStations.GetFocusedRow() != null ? (station)grvStations.GetFocusedRow() : new station();
            myForm.connection = connection;
            myForm.setStationData();
            myForm.Show();
            Cursor = Cursors.Default;
        }//repStation_ButtonClick

        private void repDev_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            stationDevices myForm = new stationDevices();
            myForm.currentItem = grvStations.GetFocusedRow() != null ? (station)grvStations.GetFocusedRow() : new station();
            myForm.connection = connection;
            myForm.setDevs();
            myForm.Show();
            Cursor = Cursors.Default;
        }//repDev_ButtonClick
    }
}
